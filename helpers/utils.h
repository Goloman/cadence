#pragma once

#include <GL/gl3w.h>
#include <SDL.h>
#include <imgui.h>

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path);
SDL_GLContext ImGui_SDLGL3_Init(const char* name, bool fullscreen, SDL_Window** window);
void ImGui_SDLGL3_Cleanup(SDL_Window* window, SDL_GLContext glcontex);
