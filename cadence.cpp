#include "cadence.h"
#include "math.h"

#include "helpers/utils.h"
#include "helpers/imgui_impl_sdl_gl3.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include <json.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <algorithm>

int cadence::App::run() {
    init();
    loop();
    cleanup();

    return 0;
}

bool showCameraWindow;
bool showListWindow;
bool showCreateWindow;
bool showColorWindow;

void cadence::App::init() {
    renderer.init(fullscreen);

    cursor.position = {0, 0, 0};
    cursorRange = 0.1f;

    state = CAMERA;

    done = false;
    hideUI = false;
    stereo = false;

    renderer.camera.position = {0, 1, 2};
    renderer.camera.rotation = {-30, 0};
    renderer.camera.scale = 1;
    renderer.camera.fov = 90.0f;
    renderer.camera.near = 0.001f;
    renderer.camera.far = 1000.0f;
    renderer.camera.stereoDistance = 0.20f;
    renderer.camera.stereoFocus = 1.00f;

    renderer.simple = true;

    movSpeed = 1.0f;
    rotSpeed = 60.0f;
    scaleSpeed = 2.0f;

    renderer.pointSize = 7;

    showCameraWindow = false;
    showListWindow = true;
    showCreateWindow = true;
    showColorWindow = false;

    filename[0] = 0;
}

bool cadence::App::eventLoop() {
    ImGuiIO& io = ImGui::GetIO();
    done = false;
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        ImGui_ImplSdlGL3_ProcessEvent(&event);
        if (event.type == SDL_QUIT)
            done = true;
        if (event.type == SDL_KEYDOWN && !io.WantCaptureKeyboard) {
            switch (event.key.keysym.sym) {
                case SDLK_h:
                    hideUI = !hideUI;
                    break;
                case SDLK_0:
                    renderer.camera.scale = 1.0f;
                    break;
                case SDLK_ESCAPE:
                    done = true;
                    break;
                case SDLK_m: {
                    if (selection.size() != 2) break;
                    if (selection.front()->type != OBJECT_POINT) break;
                    Object *p1 = selection.front();
                    Object *p2 = selection.back();
                    if (p1->virt) break;
                    if (p2->virt) break;
                    Object newPoint = Object::createPoint((p1->point->position + p2->point->position) / 2.f);
                    for (int i = 0; i < p1->parents.size(); i++) {
                        newPoint.parents.push_back(p1->parents[i]);
                    }
                    for (int i = 0; i < p2->parents.size(); i++) {
                        bool flag = false;
                        for (int j = 0; j < p1->parents.size(); j++) {
                            if (p1->parents[j] == p2->parents[i]) {
                                flag = true;
                            }
                        }
                        if (!flag) {
                            newPoint.parents.push_back(p2->parents[i]);
                        }
                    }
                    objects.push_back(newPoint);
                    Object *p3 = &objects.back();
                    for (auto &o : objects) {
                        o.replaceWith(p1, p3);
                        o.replaceWith(p2, p3);
                    }
                    p1->parents.clear();
                    p2->parents.clear();
                    p1->remove = true;
                    p2->remove = true;
                    selection.clear();
                    selection.push_back(p3);
                } break;
                case SDLK_TAB:
                    switch (state) {
                        case mode::CURVE_DESIGN:
                            switch (selection.front()->type) {
                            case OBJECT_BEZIERC0:
                                selection.front()->bezierC0->points.pop_back();
                                break;
                            case OBJECT_BEZIERC2:
                                selection.front()->bezierC2->points.pop_back();
                                break;
                            case OBJECT_BEZIERC2I:
                                selection.front()->bezierC2I->points.pop_back();
                                break;
                            case OBJECT_POINT:
                            case OBJECT_FLAKEC0:
                            case OBJECT_FLAKEC2:
                            case OBJECT_TORUS:
                            case OBJECT_GREGORY:
                            case OBJECT_INTERSECTION:
                                break;
                            }
                            Object::deleteObject(helper);
                        case mode::CAMERA:
                            state = mode::CURSOR;
                            break;
                        case mode::CURSOR:
                        case mode::CURSOR_MOVE:
                            state = mode::CAMERA;
                            break;
                    }
                    break;
                case SDLK_y: {
                    if (!selection.size()) break;
                    if (selection.front()->type != OBJECT_INTERSECTION) break;
                    selection.front()->remove = true;
                    Intersection &inter = *(selection.front()->intersection);
                    if (inter.positions.empty()) break;
                    Object *first = 0;
                    auto o = Object::createBezierC2I();
                    for (size_t i = 0; i < inter.positions.size(); i++) {
                        auto p = Object::createPoint(inter.positions[i]);
                        objects.push_back(p);
                        auto point = &objects.back();
                        o.bezierC2I->points.push_front(point);
                        if (!i) first = point;
                    }
                    if (inter.closed) o.bezierC2I->points.push_front(first);
                    o.bezierC2I->bad = true;
                    objects.push_back(o);
                    } break;
                case SDLK_t: {
                    if (!selection.size()) break;
                    if (inters.size() >= 2) break;
                    if (!selection.front()->isSurface()) break;
                    bool flag = false;
                    for (int i = 0; i < inters.size(); i++) {
                        if (inters[i] == selection.front()) {
                            flag = true;
                        }
                    }
                    if (flag) break;
                    inters.push_back(selection.front());
                    } break;
                case SDLK_g: {
                    if (!selection.size()) break;
                    if (selection.front()->type != OBJECT_FLAKEC0) break;
                    bool flag = false;
                    for (int i = 0; i < gregPrep.size(); i++) {
                        if (gregPrep[i] == selection.front()) {
                            flag = true;
                        }
                    }
                    if (flag) break;
                    gregPrep.push_back(selection.front());
                    } break;
                case SDLK_x:
                    switch (state) {
                        case mode::CAMERA:
                            break;
                        case mode::CURSOR:
                            if (selection.size()) {
                                switch (selection.front()->type) {
                                case OBJECT_BEZIERC0:
                                    state = mode::CURVE_DESIGN;
                                    helper = Object::createPoint(cursor.position);
                                    selection.front()->bezierC0->points.push_back(&helper);
                                    break;
                                case OBJECT_BEZIERC2:
                                    state = mode::CURVE_DESIGN;
                                    helper = Object::createPoint(cursor.position);
                                    selection.front()->bezierC2->points.push_back(&helper);
                                    break;
                                case OBJECT_BEZIERC2I:
                                    state = mode::CURVE_DESIGN;
                                    helper = Object::createPoint(cursor.position);
                                    selection.front()->bezierC2I->points.push_back(&helper);
                                    break;
                                case OBJECT_POINT:
                                case OBJECT_TORUS:
                                    state = mode::CURSOR_MOVE;
                                    break;
                                case OBJECT_FLAKEC0:
                                case OBJECT_FLAKEC2:
                                case OBJECT_GREGORY:
                                case OBJECT_INTERSECTION:
                                    break;
                                }
                            }
                            break;
                        case mode::CURSOR_MOVE:
                            state = mode::CURSOR;
                            break;
                        case mode::CURVE_DESIGN:
                            objects.push_back(helper);
                            std::list<Object*> *ps;

                            switch(selection.front()->type) {
                            case OBJECT_POINT:
                            case OBJECT_TORUS:
                            case OBJECT_FLAKEC0:
                            case OBJECT_FLAKEC2:
                            case OBJECT_GREGORY:
                            case OBJECT_INTERSECTION:
                                assert(false);
                                break;
                            case OBJECT_BEZIERC0:
                                ps = &(selection.front()->bezierC0->points);
                                break;
                            case OBJECT_BEZIERC2:
                                ps = &(selection.front()->bezierC2->points);
                                break;
                            case OBJECT_BEZIERC2I:
                                ps = &(selection.front()->bezierC2I->points);
                                break;
                            }

                            ps->pop_back();
                            ps->push_back(&objects.back());
                            helper = Object::createPoint(cursor.position);
                            ps->push_back(&helper);
                            break;
                    }
                    break;
                case SDLK_SPACE:
                    if (!(event.key.keysym.mod & KMOD_CTRL))
                        selection.clear();
                    Object* temp = nullptr;
                    float best = cursorRange * cursorRange;
                    for (auto &o : objects) {
                        if (o.type != OBJECT_POINT) continue;
                        auto diff = cursor.position - o.point->position;
                        auto d2 = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
                        if (d2 > best) continue;
                        temp = &o;
                        best = d2;
                    }

                    for (auto o : virtuals) {
                        if (o->type != OBJECT_POINT) continue;
                        auto diff = cursor.position - o->point->position;
                        auto d2 = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
                        if (d2 > best) continue;
                        temp = o;
                        best = d2;
                    }
                    if (temp) selectObject(temp);
                    break;
            }
        }
    }
    return done;
}

void cadence::App::diagnosticWindow() {
    ImGuiIO& io = ImGui::GetIO();
    ImVec2 wp = {io.DisplaySize.x - 10, 30};
    ImVec2 wpp = {1, 0};
    ImGui::SetNextWindowPos(wp, ImGuiCond_Always, wpp);
    ImGui::SetNextWindowBgAlpha(0.3f);
    if (ImGui::Begin("Diagnostic window", nullptr, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoFocusOnAppearing|ImGuiWindowFlags_NoNav)) {
        ImGui::Text("%.1f", io.Framerate);
        const char* text = "null mode";
        switch(state) {
            case mode::CAMERA:
                text = "camera mode";
                break;
            case mode::CURSOR:
                text = "cursor mode";
                break;
            case mode::CURSOR_MOVE:
                text = "move mode";
                break;
            case mode::CURVE_DESIGN:
                text = "curve design";
                break;
        }
        ImGui::Text("%s", text);
        ImGui::Text("[%f, %f, %f]", cursor.position[0], cursor.position[1], cursor.position[2]);

        auto pos = renderer.camera.screenPos(cursor.position);
        ImGui::Text("[%d, %d]", pos[0], pos[1]);

        ImGui::End();
    }
}

void cadence::App::selectObject(Object* o) {
    bool inSelection = false;
    switch (state) {
    case CAMERA:
    case CURSOR:
    case CURSOR_MOVE:
        if (!o) {
            selection.clear();
            return;
        }
        if (!ImGui::GetIO().KeyCtrl || o->type != OBJECT_POINT || selection.size() == 0 || selection.front()->type != OBJECT_POINT)
            selection.clear();
        for (auto it = selection.begin(); it != selection.end(); ++it)
            inSelection |= *it == o;
        if (inSelection) {
            selection.remove(o);
        } else {
            selection.push_front(o);
        }
        break;
    case CURVE_DESIGN:
        {
        if (!o) return;
        if (o->type != OBJECT_POINT) break;

        std::list<Object *> *ps;

        switch(selection.front()->type) {
            case OBJECT_POINT:
            case OBJECT_TORUS:
            case OBJECT_FLAKEC0:
            case OBJECT_FLAKEC2:
            case OBJECT_GREGORY:
            case OBJECT_INTERSECTION:
                assert(false);
                break;
            case OBJECT_BEZIERC0:
                ps = &(selection.front()->bezierC0->points);
                break;
            case OBJECT_BEZIERC2:
                ps = &(selection.front()->bezierC2->points);
                break;
            case OBJECT_BEZIERC2I:
                ps = &(selection.front()->bezierC2I->points);
                break;
        }

        ps->pop_back();
        ps->push_back(o);
        ps->push_back(&helper);
        }
        break;
    }

}

void cadence::App::loop() {
    ImGuiIO& io = ImGui::GetIO();
    while (!done) {
        renderer.camera.updateMatrices();

        done = eventLoop();

        renderer.startFrame();

        mainMenuBar();
        diagnosticWindow();
        cameraWindow();
        creationWindow();
        listWindow();

        float dt = io.DeltaTime;

        virtuals.clear();
        for (Object &o : objects) {
            if (o.type == OBJECT_BEZIERC2 && o.bezierC2->bezierMode) {
                for (Object &p : o.bezierC2->virtuals) {
                    virtuals.push_back(&p);
                }
            }
        }


        if (!selection.size() && state == mode::CURSOR_MOVE) {
            state = mode::CURSOR;
        }
        if (!io.WantCaptureKeyboard) {
            handleKeyboard(dt, renderer.camera.forward, renderer.camera.side);
        }

        if (!io.WantCaptureMouse) {
            if (io.MouseClicked[0]) {
                glm::ivec2 mousePos = {io.MouseClickedPos[0].x, io.MouseClickedPos[0].y};
                Object *s = nullptr;
                int best = 400;
                for (auto &object : objects) {
                    if (object.type != OBJECT_POINT) continue;
                    auto pos = renderer.camera.screenPos(object.point->position);
                    if (pos.x < 0) continue;
                    auto delta = pos - mousePos;
                    int d2 = delta.x * delta.x + delta.y * delta.y;
                    if (d2 < best) {
                        best = d2;
                        s = &object;
                    }
                }
                for (auto object : virtuals) {
                    if (object->type != OBJECT_POINT) continue;
                    auto pos = renderer.camera.screenPos(object->point->position);
                    if (pos.x < 0) continue;
                    auto delta = pos - mousePos;
                    int d2 = delta.x * delta.x + delta.y * delta.y;
                    if (d2 < best) {
                        best = d2;
                        s = object;
                    }
                }
                selectObject(s);
            }
        }

        for (auto &o : objects) {
            Object::update(o);
        }

        selection.remove_if([](cadence::Object* o){
            return o->remove;
        });

        for (int i = 0; i < gregPrep.size();) {
            if (!((gregPrep[i])->remove)) {
                i++;
                continue;
            }
            for (int j = i; j < gregPrep.size() - 1; j++) {
                gregPrep[j] = gregPrep[j+1];
            }
            gregPrep.pop_back();
        }

        for (int i = 0; i < inters.size();) {
            if (!((inters[i])->remove)) {
                i++;
                continue;
            }
            for (int j = i; j < inters.size() - 1; j++) {
                inters[j] = inters[j+1];
            }
            inters.pop_back();
        }

        for (auto &o : objects) {
            Object::cleanup(o);
        }

        objects.remove_if([](cadence::Object o){
            if (o.remove)
                Object::deleteObject(o);
            return o.remove;
        });


        virtuals.clear();
        for (Object &o : objects) {
            if (o.type == OBJECT_BEZIERC2 && o.bezierC2->bezierMode) {
                for (Object &p : o.bezierC2->virtuals) {
                    virtuals.push_back(&p);
                }
            }
        }


        if (stereo) {
            renderer.viewMatrix = &renderer.camera.viewMatrixL;
            scheduleObjects();
            renderer.scheduleCursor(cursor, cursorRange);
            renderer.draw(renderer.camera.viewMatrixL, renderer.camera.projectionMatrixL, {0.7f, 0.0f, 0.0f});
            renderer.clearBuffers();

            renderer.viewMatrix = &renderer.camera.viewMatrixR;
            scheduleObjects();
            renderer.scheduleCursor(cursor, cursorRange);
            renderer.drawAdd(renderer.camera.viewMatrixR, renderer.camera.projectionMatrixR, {0.0f, 0.0f, 1.0f});
            renderer.clearBuffers();
        } else {
            renderer.viewMatrix = &renderer.camera.viewMatrix;
            scheduleObjects();
            renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, {0.7, 0.7, 0.7});

            if (selection.size()) {
                renderer.clearBuffers();
                for (auto o : selection)
                    renderer.schedule(*o);
                renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, {0.7, 0.4, 0});

                renderer.clearBuffers();
                renderer.schedule(*selection.front());
                renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, {1, 0.5, 0});
            }

            if (gregPrep.size()) {
                renderer.clearBuffers();
                for (auto o : gregPrep)
                    renderer.schedule(*o);
                renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, {0.6, 1.0, 0});
            }

            if (inters.size()) {
                renderer.clearBuffers();
                for (auto o : inters) {
                    renderer.schedule(*o);
                    glm::vec2 uv = o->uvFromPos(cursor.position);
                    /*
                    static glm::vec2 temp = {0, 0};
                    ImGui::DragFloat2("uvs", &(temp.x), 0.01f);
                    glm::vec3 pos = o->evaluate(temp);
                    /*/
                    glm::vec3 pos = o->evaluate(uv);
                    //*/
                    pos = renderer.viewed(pos);
                    renderer.points.push_back(pos);
                    renderer.scheduleLine(pos, renderer.viewed(cursor.position));
                    if (inters.size() == 1) {
                        glm::vec2 st = o->uvFromPos(cursor.position, uv, parametricDistance);
                        pos = o->evaluate(st);
                        pos = renderer.viewed(pos);
                        renderer.points.push_back(pos);
                        renderer.scheduleLine(pos, renderer.viewed(cursor.position));
                    }
                }

                if (!inters.empty() && intersectionPreview) {
                    Object &o1 = *inters.front();
                    Object &o2 = *inters.back();

                    glm::vec3 pos = cursor.position;
                    glm::vec3 start = {0, 0, 0};
                    float firstStep = 0;

                    glm::vec2 uv = o1.uvFromPos(pos);
                    glm::vec2 st;
                    if (inters.size() == 2) {
                        st = o2.uvFromPos(pos);
                    } else {
                        st = o2.uvFromPos(pos, uv, parametricDistance);
                    }
                    bool out = false;

                    for (int i = 0; i < 5; i++) {
                        glm::vec4 grad = gradient(o1, o2, uv, st);
                        uv -= glm::vec2(grad.x, grad.y);
                        st -= glm::vec2(grad.z, grad.w);
                        auto t1 = o1.isOutside(uv);
                        auto t2 = o2.isOutside(st);
                        if (t1.x || t1.y || t2.x || t2.y) {
                            out = true;
                            break;
                        }
                    }

                    if (!out) {
                        for (int j = 0; j < previewCount; j++) {
                            glm::vec3 p1 = o1.evaluate(uv);
                            glm::vec3 p2 = o2.evaluate(st);
                            pos = (p1 + p2) / 2.f;
                            renderer.points.push_back(renderer.viewed(pos));
                            renderer.scheduleLine(renderer.viewed(pos), renderer.viewed(p1));
                            renderer.scheduleLine(renderer.viewed(pos), renderer.viewed(p2));
                            if (j == 0) start = pos;
                            if (j == 1) firstStep = glm::length(pos - start);
                            if (j > 1 && glm::length(pos - start) < firstStep * 1.2f) break;
                            glm::vec3 n1 = o1.evaluateNormal(uv);
                            glm::vec3 n2 = o2.evaluateNormal(st);
                            glm::vec3 n = glm::cross(n1, n2);
                            n = glm::normalize(n);
                            float d = glm::dot(n, pos);
                            d += interDelta;

                            for (int i = 0; i < 3; i++) {
                                glm::mat4 inv = glm::inverse(jacobi(o1, o2, uv, st, n));
                                glm::vec3 p1 = o1.evaluate(uv);
                                glm::vec3 p2 = o2.evaluate(st);
                                float dist = glm::dot(p1 - n * d, n);
                                glm::vec4 value = {p1 - p2, dist};
                                glm::vec4 delta = inv * value;
                                delta *= 0.02f;
                                uv -= glm::vec2(delta.x, delta.y);
                                st -= glm::vec2(delta.z, delta.w);
                                o1.isOutside(uv);
                                o2.isOutside(st);
                            }
                        }
                    }

                }
                renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, {0.6, 0.0, 1.0});
            }

            renderer.clearBuffers();
            renderer.scheduleCursor(cursor, cursorRange);
            renderer.draw(renderer.camera.viewMatrix, renderer.camera.projectionMatrix, (state == CURSOR_MOVE) ? glm::vec3(0, 1, 1) : glm::vec3(0, 0.3, 1));
            renderer.clearBuffers();
        }

        renderer.endFrame();
    }
}

void cadence::App::cleanup() {
    renderer.cleanup();
}

void cadence::App::scheduleObjects() {
    std::list<cadence::Object>::iterator it;
    for(it = objects.begin(); it != objects.end(); ++it) {
        //object->drawPrep();
        renderer.schedule(*it);
    }


    for(Object *o : virtuals) {
        renderer.schedule(*o);
    }
    //renderer.schedule(cursor);
}

void cadence::App::mainMenuBar() {
    if (hideUI) return;

    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            ImGui::InputText("Filename", filename, 512);
            if (ImGui::MenuItem("Load file")) {
                loadFile();
            }
            if (ImGui::MenuItem("Save file")) {
                saveFile();
            }
            ImGui::Separator();
            if (ImGui::MenuItem("Create model")){
                createHeli();
            }
            if (ImGui::MenuItem("Clear scene")) {
                clearScene();
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("View")) {
            ImGui::MenuItem("Object creation", "", &showCreateWindow);
            ImGui::MenuItem("Object list", "", &showListWindow);
            ImGui::MenuItem("Camera settings", "", &showCameraWindow);
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void cadence::App::clearScene() {
    for (auto &o : objects) {
        o.remove = true;
    }
}

void cadence::App::saveFile() {
    nlohmann::json out;

    nlohmann::json points = nlohmann::json::array();
    nlohmann::json curvesC0 = nlohmann::json::array();
    nlohmann::json curvesC2 = nlohmann::json::array();
    nlohmann::json curvesC2I = nlohmann::json::array();
    nlohmann::json surfacesC0 = nlohmann::json::array();
    nlohmann::json surfacesC2 = nlohmann::json::array();
    nlohmann::json toruses = nlohmann::json::array();

    std::vector<Object*> pointPointers;
    for(auto &o : objects) {
        if (o.type != OBJECT_POINT) {
            continue;
        }

        pointPointers.push_back(&o);
    }

    for(auto &o : objects) {
        switch(o.type) {
        case OBJECT_POINT: {
            nlohmann::json p;
            p["name"] = o.name;
            p["x"] = o.point->position.x;
            p["y"] = o.point->position.y;
            p["z"] = o.point->position.z;
            points.push_back(p);
        } break;
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
        break;
        case OBJECT_FLAKEC0: {
            nlohmann::json f;
            f["name"] = o.name;
            f["cylinder"] = o.flakeC0->cylinder;
            f["flakeU"] = o.flakeC0->width;
            f["flakeV"] = o.flakeC0->height;
            f["u"] = o.flakeC0->hRes;
            f["v"] = o.flakeC0->vRes;
            nlohmann::json ps = nlohmann::json::array();
            for (int i = 0; i < o.flakeC0->height * 3 + 1; i++) {
                nlohmann::json p2s = nlohmann::json::array();
                for (int j = 0; j < o.flakeC0->width * 3 + 1; j++) {
                    auto ref = o.flakeC2->points[i * (o.flakeC0->width * 3 + 1) + j];
                    unsigned int index;

                    for (index = 0; pointPointers[index] != ref; index++);

                    p2s.push_back(index);

                }
                ps.push_back(p2s);
            }
            f["points"] = ps;

            surfacesC0.push_back(f);
        } break;
        case OBJECT_FLAKEC2: {
            nlohmann::json f;
            f["name"] = o.name;
            f["cylinder"] = o.flakeC2->cylinder;
            f["flakeU"] = o.flakeC2->width;
            f["flakeV"] = o.flakeC2->height;
            f["u"] = o.flakeC2->hRes;
            f["v"] = o.flakeC2->vRes;
            nlohmann::json ps = nlohmann::json::array();
            for (int i = 0; i < o.flakeC2->height + 3; i++) {
                nlohmann::json p2s = nlohmann::json::array();
                for (int j = 0; j < o.flakeC2->width + 3; j++) {
                    auto ref = o.flakeC2->points[i * (o.flakeC2->width + 3) + j];
                    unsigned int index;

                    for (index = 0; pointPointers[index] != ref; index++);

                    p2s.push_back(index);

                }
                ps.push_back(p2s);
            }
            f["points"] = ps;

            surfacesC2.push_back(f);
        } break;
        case OBJECT_TORUS: {
            nlohmann::json t, pos, rot, sca;
            t["name"] = o.name;
            pos["x"] = o.torus->position.x;
            pos["y"] = o.torus->position.y;
            pos["z"] = o.torus->position.z;
            rot["x"] = o.torus->rotation.x;
            rot["y"] = o.torus->rotation.y;
            rot["z"] = o.torus->rotation.z;
            sca["x"] = o.torus->scale.x;
            sca["y"] = o.torus->scale.y;
            sca["z"] = o.torus->scale.z;
            t["center"] = pos;
            t["rotation"] = rot;
            t["scale"] = sca;
            t["R"] = o.torus->major;
            t["r"] = o.torus->minor;
            t["v"] = o.torus->majRes;
            t["u"] = o.torus->minRes;
            toruses.push_back(t);
        } break;
        }

    }

    out["points"] = points;
    out["curvesC0"] = curvesC0;
    out["curvesC2"] = curvesC2;
    out["curvesC2I"] = curvesC2I;
    out["surfacesC0"] = surfacesC0;
    out["surfacesC2"] = surfacesC2;
    out["toruses"] = toruses;

    std::ofstream ostr(filename);
    ostr << out.dump(2, ' ');;
    ostr.close();

}

void cadence::App::loadFile() {
    for (auto &o : objects) {
        o.remove = true;
    }

    for (auto &o : objects) {
        Object::update(o);
    }

    selection.remove_if([](cadence::Object* o){
        return o->remove;
    });

    for (auto &o : objects) {
        Object::cleanup(o);
    }

    objects.remove_if([](cadence::Object o){
        if (o.remove)
            Object::deleteObject(o);
        return o.remove;
    });

    std::ifstream i;
    nlohmann::json j;

    try {
        i.open(filename);
        i >> j;
    } catch (...) {
        return;
    }

    auto items = j["points"];
    for (auto p : items) {
        float x, y, z;
        x = p["x"].get<float>();
        y = p["y"].get<float>();
        z = p["z"].get<float>();
        Object o = Object::createPoint({x, y, z}, true);
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "%s", p["name"].get<std::string>().data());
        objects.push_back(o);
    }

    items = j["toruses"];
    for (auto t : items) {
        float x, y, z;
        x = t["center"]["x"].get<float>();
        y = t["center"]["y"].get<float>();
        z = t["center"]["z"].get<float>();
        glm::vec3 position = {x, y, z};
        x = t["rotation"]["x"].get<float>();
        y = t["rotation"]["y"].get<float>();
        z = t["rotation"]["z"].get<float>();
        glm::vec3 rotation = {x, y, z};
        x = t["scale"]["x"].get<float>();
        y = t["scale"]["y"].get<float>();
        z = t["scale"]["z"].get<float>();
        glm::vec3 scale = {x, y, z};
        float R = t["R"].get<float>();
        float r = t["r"].get<float>();
        float uRes = t["u"].get<int>();
        float vRes = t["v"].get<int>();
        Object o = Object::createTorus(position, rotation, scale, r, R, uRes, vRes);
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "%s", t["name"].get<std::string>().data());
        objects.push_back(o);
    }

    items = j["curvesC2"];
    for (auto c : items) {
        Object o = Object::createBezierC2();
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "%s", c["name"].get<std::string>().data());
        auto ps = c["pointsId"];
        for (auto p : ps) {
            int index = p.get<int>();
            auto iter = objects.begin();
            while (index--) iter++;
            o.bezierC2->points.push_back(&*iter);
        }
        objects.push_back(o);
    }

    items = j["surfacesC0"];
    for (auto f : items) {
        Object o = Object::createFlakeC0();
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "%s", f["name"].get<std::string>().data());
        o.flakeC0->hRes = f["u"].get<int>();
        o.flakeC0->vRes = f["v"].get<int>();
        o.flakeC0->cylinder = f["cylinder"].get<bool>();
        auto px = f["points"];
        for (auto py : px) {
            o.flakeC0->height = (px.size() - 1) / 3;
            o.flakeC0->width = (py.size()-1) / 3;
            for (auto p : py) {
                int index = p.get<int>();
                auto iter = objects.begin();
                while (index--) iter++;
                o.flakeC0->points.push_back(&*iter);
            }
        }
        objects.push_back(o);
        auto p = &objects.back();
        for (auto q : p->flakeC0->points) {
            bool flag = false;

            for (auto a : q->parents) {
                if (a == p) flag = true;
            }

            if (!flag) q->parents.push_back(p);
        }
    }

    items = j["surfacesC2"];
    for (auto f : items) {
        Object o = Object::createFlakeC2();
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "%s", f["name"].get<std::string>().data());
        o.flakeC2->hRes = f["u"].get<int>();
        o.flakeC2->vRes = f["v"].get<int>();
        o.flakeC2->cylinder = f["cylinder"].get<bool>();
        auto px = f["points"];
        for (auto py : px) {
            o.flakeC2->height = px.size() - 3;
            o.flakeC2->width = py.size() - 3;
            for (auto p : py) {
                int index = p.get<int>();
                auto iter = objects.begin();
                while (index--) iter++;
                o.flakeC2->points.push_back(&*iter);
            }
        }
        objects.push_back(o);
    }
}

void cadence::App::listWindow() {
    if (hideUI || !showListWindow)
        return;

    static bool showPoints = false;
    ImGui::Begin("Object list");
    ImGui::Checkbox("Show points", &showPoints);
    ImGui::Separator();
    for (auto &o : objects) {
        if (!showPoints && o.type == OBJECT_POINT) continue;
        char buf[OBJECT_MAX_NAME_LENGTH + 64];
        snprintf(buf, OBJECT_MAX_NAME_LENGTH + 64, "%s###%p", o.name, &o);
        bool selected = false;
        for (auto p : selection) {
            if (p != &o) continue;
            selected = true;
            break;
        }

        if (ImGui::Selectable(buf, selected)) {
            selectObject(&o);
        }
    }
    ImGui::End();
    if (selection.size()) {
        ImGui::Begin("Selection");
        Object::drawControls(*selection.front(), state);
        ImGui::End();
    }
}

void cadence::App::creationWindow() {
    if (hideUI || !showCreateWindow || state == CURVE_DESIGN)
        return;

    ImGui::Begin("Create Object", nullptr, 0);

    static int item = 0;
    ImGui::Combo("##creationselect", &item, "point\0torus\0bezier c0\0bezier c2\0bezier c2i\0flake c0\0flake c2\0gregory\0intersection\0\0");

    ImGui::Separator();

    if (item == 0) {
        if(ImGui::Button("Create")) {
            objects.push_back(Object::createPoint(cursor.position));
        }
        ImGui::SameLine();
        if(ImGui::Button("Create at origin")) {
            objects.push_back(Object::createPoint({0, 0, 0}));
        }
    }

    if (item == 1) {

        if(ImGui::Button("Create")) {
            objects.push_back(Object::createTorus(cursor.position, {0, 0, 0}, {1, 1, 1}, 0.5, 2, 24, 12));
        }
        if(ImGui::Button("Create at origin")) {
            objects.push_back(Object::createTorus({0, 0, 0}, {0, 0, 0}, {1, 1, 1}, 0.5, 2, 64, 32));
        }
    }

    if (item == 2) {
        if (ImGui::Button("Create") && selection.size() && selection.front()->type == OBJECT_POINT) {
            auto o = Object::createBezierC0();
            for (auto p : selection)
                o.bezierC0->points.push_front(p);
            objects.push_back(o);
        }
        if (ImGui::Button("Create and extend") && ((selection.size() && selection.front()->type == OBJECT_POINT) || !selection.size())) {
            auto o = Object::createBezierC0();
            for (auto p : selection)
                o.bezierC0->points.push_front(p);
            objects.push_back(o);
            selection.clear();
            selection.push_back(&objects.back());
            helper = Object::createPoint(cursor.position);
            o.bezierC0->points.push_back(&helper);
            state = mode::CURVE_DESIGN;
        }
    }

    if (item == 3) {
        if (ImGui::Button("Create") && selection.size() && selection.front()->type == OBJECT_POINT) {
            auto o = Object::createBezierC2();
            for (auto p : selection)
                o.bezierC2->points.push_front(p);
            objects.push_back(o);
        }
        if (ImGui::Button("Create and extend") && ((selection.size() && selection.front()->type == OBJECT_POINT) || !selection.size())) {
            auto o = Object::createBezierC2();
            for (auto p : selection)
                o.bezierC2->points.push_front(p);
            objects.push_back(o);
            selection.clear();
            selection.push_back(&objects.back());
            helper = Object::createPoint(cursor.position);
            o.bezierC2->points.push_back(&helper);
            state = mode::CURVE_DESIGN;
        }
    }

    if (item == 4) {
        if (ImGui::Button("Create") && selection.size() && selection.front()->type == OBJECT_POINT) {
            auto o = Object::createBezierC2I();
            for (auto p : selection)
                o.bezierC2I->points.push_front(p);
            objects.push_back(o);
        }
        if (ImGui::Button("Create and extend") && ((selection.size() && selection.front()->type == OBJECT_POINT) || !selection.size())) {
            auto o = Object::createBezierC2I();
            for (auto p : selection)
                o.bezierC2I->points.push_front(p);
            objects.push_back(o);
            selection.clear();
            selection.push_back(&objects.back());
            helper = Object::createPoint(cursor.position);
            o.bezierC2I->points.push_back(&helper);
            state = mode::CURVE_DESIGN;
        }
    }

    if (item == 5) {
        static int width = 1;
        static int height = 1;

        static float w = 1;
        static float h = 1;

        static bool cylinder = false;

        ImGui::InputInt("Width count", &width);
        ImGui::InputInt("Height count", &height);

        if (width < 1) width = 1;
        if (height < 1) height = 1;

        ImGui::InputFloat("Width / radius", &w);
        ImGui::InputFloat("Height", &h);

        ImGui::Checkbox("Cylinder", &cylinder);

        static int orientation = 1;
        ImGui::Combo("##orientationselect", &orientation, "x\0y\0z\0\0");

        bool create = false;
        bool atCursor = false;

        if (ImGui::Button("Create")) {
            create = true;
        }

        if (ImGui::Button("Create at cursor")) {
            create = true;
            atCursor = true;
        }

        if (create && !selection.size()) {
            createFlakeC0(width, height, w, h, cylinder, atCursor ? cursor.position : glm::vec3(0, 0, 0), orientation);
        }
    }

    if (item == 6) {
        static int width = 1;
        static int height = 1;

        static float w = 1;
        static float h = 1;

        static bool cylinder = false;

        ImGui::InputInt("Width count", &width);
        ImGui::InputInt("Height count", &height);

        if (width < 1) width = 1;
        if (height < 1) height = 1;

        ImGui::InputFloat("Width / radius", &w);
        ImGui::InputFloat("Height", &h);

        ImGui::Checkbox("Cylinder", &cylinder);
        if (width < 3) cylinder = false;

        static int orientation = 1;
        ImGui::Combo("##orientationselect", &orientation, "x\0y\0z\0\0");

        bool create = false;
        bool atCursor = false;

        if (ImGui::Button("Create")) {
            create = true;
        }

        if (ImGui::Button("Create at cursor")) {
            create = true;
            atCursor = true;
        }

        if (create && !selection.size()) {
            createFlakeC2(width, height, w, h, cylinder, atCursor ? cursor.position : glm::vec3(0, 0, 0), orientation);
        }
    }

    if (item == 7) {
        if (ImGui::Button("Clear")) {
            gregPrep.clear();
        }
        for (auto o : gregPrep) {
            ImGui::Text("%s", o->name);
        }
        ImGui::Separator();
        if (ImGui::Button("Make")) {
            int count = gregPrep.size();
            std::vector<Object*> flakes(count);
            std::vector<size_t> lasts(count);
            std::vector<size_t> nexts(count);

            bool cyclePresent = true;

            for (int i = 0; i < count; i++) {
                flakes[i] = gregPrep[i];


                int prevIndex = (i - 1) % count;
                if (prevIndex < 0) prevIndex += count;
                int nextIndex = (i + 1) % count;

                bool found = false;
                int j;
                for (j = 0; j < flakes[i]->flakeC0->points.size(); j++) {
                    auto p = flakes[i]->flakeC0->points[j];
                    bool goodParent = false;
                    for (int k = 0; k < p->parents.size(); k++) {
                        if (p->parents[k] == gregPrep[nextIndex]) {
                            goodParent = true;
                        }
                    }
                    if (!goodParent) continue;
                    found = true;
                    break;
                }
                if (!found) {
                    cyclePresent = false;
                    break;
                }
                nexts[i] = j;


                found = false;
                for (j = 0; j < flakes[i]->flakeC0->points.size(); j++) {
                    auto p = flakes[i]->flakeC0->points[j];
                    bool goodParent = false;
                    for (int k = 0; k < p->parents.size(); k++) {
                        if (p->parents[k] == gregPrep[prevIndex]) {
                            goodParent = true;
                        }
                    }
                    if (!goodParent) continue;
                    found = true;
                    break;
                }
                if (!found) {
                    cyclePresent = false;
                    break;
                }
                lasts[i] = j;
            }

            if (cyclePresent) {
                bool cycleGood = true;

                for (int i = 0; i < count; i++) {
                    int width = flakes[i]->flakeC0->width * 3 + 1;
                    int height = flakes[i]->flakeC0->height * 3 + 1;
                    if (lasts[i] % width % 3 != 0 || nexts[i] % width % 3 != 0 ||
                        lasts[i] / height % 3 != 0 || nexts[i] / height % 3 != 0) {
                        cycleGood = false;
                        break;
                    }
                    int lx = lasts[i] % width / 3;
                    int ly = lasts[i] / height / 3;
                    int nx = nexts[i] % width / 3;
                    int ny = nexts[i] / height / 3;

                    int diff = (lx - nx) * (lx - nx) + (ly - ny) * (ly - ny);

                    if (diff != 1) {
                        cycleGood = false;
                        break;
                    }
                }

                if (cycleGood) {
                auto o = Object::createGregory();


                for (auto p : gregPrep) {
                    o.parents.push_back(p);
                }

                o.gregory->flakes = flakes;
                o.gregory->pointIndicesLast = lasts;
                o.gregory->pointIndicesNext = nexts;
                o.gregory->hRes = 3;
                o.gregory->vRes = 3;
                objects.push_back(o);
                gregPrep.clear();
                }
            }
        }
    }
    if (item == 8) {
        if (ImGui::Button("Clear")) {
            inters.clear();
        }
        for (auto o : inters) {
            ImGui::Text("%s", o->name);
        }
        ImGui::Separator();
        ImGui::DragFloat("Move delta", &interDelta, 0.01);
        ImGui::DragInt("Max iterations", &intersectionMaxCount);
        ImGui::Checkbox("Preview", &intersectionPreview);
        ImGui::DragInt("Preview iterations", &previewCount);
        ImGui::DragFloat("Self-intersection margin", &parametricDistance, 0.01f);
        ImGui::DragFloat("Normal threshold", &normalThreshold, 0.01f);
        if (ImGui::Button("Make")) {
            createIntersection();
        }
    }
    ImGui::End();
}

struct bitmap {
    uint8_t *data;
    size_t w;

    void point(size_t x, size_t y, uint8_t col);
    uint8_t point(size_t x, size_t y);
    void bresenhamLow(int x0, int y0, int x1, int y1, uint8_t col);
    void bresenhamHigh(int x0, int y0, int x1, int y1, uint8_t col);
    void bresenham(int x0, int y0, int x1, int y1, uint8_t col);
    void flood(int x, int y, uint8_t old, uint8_t next, bool uLoop, bool vLoop);
};

void bitmap::flood(int x, int y, uint8_t old, uint8_t next, bool uLoop, bool vLoop) {
    std::vector<glm::ivec2> queue;
    queue.push_back({x, y});

    while (!queue.empty()) {
        auto p = queue.back();
        if (point(p.x, p.y) != old) {
            queue.pop_back();
            continue;
        }
        point(p.x, p.y, next);
        int x0 = p.x - 1;
        int y0 = p.y - 1;
        int x1 = p.x + 1;
        int y1 = p.y + 1;
        if (x0 <  0) x0 += uLoop ? w : 1;
        if (y0 <  0) y0 += vLoop ? w : 1;
        if (x1 >= w) x1 -= uLoop ? w : 1;
        if (y1 >= w) y1 -= vLoop ? w : 1;
        int xx = p.x;
        int yy = p.y;
        queue.pop_back();
        queue.push_back({xx, y0});
        queue.push_back({xx, y1});
        queue.push_back({x0, yy});
        queue.push_back({x1, yy});
    }

}

void bitmap::point(size_t x, size_t y, uint8_t col) {
    size_t i = y * w + x;
    data[i] = col;
}

uint8_t bitmap::point(size_t x, size_t y) {
    size_t i = y * w + x;
    return data[i];
}

void bitmap::bresenham(int x0, int y0, int x1, int y1, uint8_t col) {
  if (abs(y1 - y0) < abs(x1 - x0)) {
    if (x0 > x1)
      bresenhamLow(x1, y1, x0, y0, col);
    else
      bresenhamLow(x0, y0, x1, y1, col);
  } else {
    if (y0 > y1)
      bresenhamHigh(x1, y1, x0, y0, col);
    else
      bresenhamHigh(x0, y0, x1, y1, col);
  }
}

void bitmap::bresenhamLow(int x0, int y0, int x1, int y1, uint8_t col) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    int yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    int D = 2*dy - dx;
    int y = y0;
    for (int x = x0; x <= x1; x++) {
        point(x,y,col);
        if (D > 0) {
            y = y + yi;
            D = D - 2*dx;
        }
        D = D + 2*dy;
    }
}


void bitmap::bresenhamHigh(int x0, int y0, int x1, int y1, uint8_t col) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    int xi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    int D = 2*dx - dy;
    int x = x0;
    for (int y = y0; y <= y1; y++) {
        point(x,y,col);
        if (D > 0) {
            x = x + xi;
            D = D - 2*dy;
        }
        D = D + 2*dx;
    }
}

bool isTrimming(std::vector<glm::vec2> uvs, glm::vec2 range, bool loops, bool uLoop, bool vLoop) {
    int size = 1024;
    int s2 = size / 2;
    bitmap M;
    M.w = size;
    M.data = (uint8_t *)calloc(size * size, 1);
    for (int i = 0; i < uvs.size(); i++) {
        if (i == uvs.size() - 1 && !loops) break;
        auto uv0 = uvs[i];
        auto uv1 = uvs[(i+1)%uvs.size()];


        glm::ivec2 p0 = uv0 / range * (float)size;
        glm::ivec2 p1 = uv1 / range * (float)size;
        if (p0.x < 0) p0.x = 0;
        if (p0.y < 0) p0.y = 0;
        if (p1.x < 0) p1.x = 0;
        if (p1.y < 0) p1.y = 0;
        if (p0.x >= size) p0.x = size-1;
        if (p0.y >= size) p0.x = size-1;
        if (p1.x >= size) p1.y = size-1;
        if (p1.y >= size) p1.y = size-1;
        bool xflag = abs(p0.x - p1.x) > s2;
        bool yflag = abs(p0.y - p1.y) > s2;
        if (xflag || yflag) {
            glm::ivec2 p02 = (p0 + p1) / 2;
            glm::ivec2 p12 = (p0 + p1) / 2;
            if (xflag) {
                if (p0.x > s2) {
                    p02.x = size-1;
                    p12.x = 0;
                } else {
                    p02.x = 0;
                    p12.x = size-1;
                }
            }
            if (yflag) {
                if (p0.y > s2) {
                    p02.y = size-1;
                    p12.y = 0;
                } else {
                    p02.y = 0;
                    p12.y = size-1;
                }
            }
            M.bresenham(p0.x, p0.y, p02.x, p02.y, 1);
            M.bresenham(p1.x, p1.y, p12.x, p12.y, 1);
        } else {
            M.bresenham(p0.x, p0.y, p1.x, p1.y, 1);
        }

    }

    bool found = false;
    int x, y;
    for (x = 0; x < size; x++) {
        for (y = 0; y < size; y++) {
            if (!M.point(x, y)) {
                found = true;
                break;
            }
        }
        if (found) break;
    }

    if (!found) {
        free(M.data);
        return false;
    }

    M.flood(x, y, 0, 2, uLoop, vLoop);

    found = false;
    for (x = 0; x < size; x++) {
        for (y = 0; y < size; y++) {
            if (!M.point(x, y)) {
                found = true;
                break;
            }
        }
        if (found) break;
    }

    free(M.data);
    return found;

    for (int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++)
            printf("%hhu", M.point(x, y));
        printf("\n");
    }
    printf("\n");

    free(M.data);
    return true;

}

void cadence::App::createIntersection() {
    if (inters.empty()) return;

    if (inters.size() == 1 && inters.front()->type == OBJECT_TORUS) {
        Object &o = *inters.front();
        Torus &t = *o.torus;
        if (t.major >= t.minor) {
            inters.clear();
            return;
        }

        std::vector<glm::vec2> uvs;
        std::vector<glm::vec2> sts;
        std::vector<glm::vec3> pos;

        float u = acos(t.major / t.minor);

        float pi = glm::pi<float>();

        for (float v = 0.1; v < pi * 2; v += 1) {
            uvs.push_back({pi + u, v});
            sts.push_back({pi - u, v});
            pos.push_back(o.evaluate({pi + u, v}));
        }
        uvs.push_back({pi + u, 0.1});
        sts.push_back({pi - u, 0.1});
        pos.push_back(o.evaluate({pi + u, 0.1}));

        o.intersected = true;
        o.uvs.insert(o.uvs.end(), uvs.begin(), uvs.end());
        o.uvEnds.push_back(o.uvs.size()-1);
        o.uvs.insert(o.uvs.end(), sts.begin(), sts.end());
        o.uvEnds.push_back(o.uvs.size()-1);

        auto i = Object::createIntersection();
        i.intersection->uvRange = o.uvRange();
        i.intersection->stRange = o.uvRange();
        i.intersection->uvs = uvs;
        i.intersection->sts = sts;
        i.intersection->positions = pos;
        i.intersection->closed = true;
        i.intersection->uvTrim = true;
        i.intersection->stTrim = true;
        snprintf(i.intersection->uvName, OBJECT_MAX_NAME_LENGTH, "%s", o.name);
        snprintf(i.intersection->stName, OBJECT_MAX_NAME_LENGTH, "%s", o.name);
        objects.push_back(i);

        inters.clear();
        return;
    }

    Object &o1 = *inters.front();
    Object &o2 = *inters.back();

    glm::vec2 uvRange = o1.uvRange();
    glm::vec2 stRange = o2.uvRange();
    std::vector<glm::vec2> uvs;
    std::vector<glm::vec2> sts;
    std::vector<glm::vec3> positions;
    bool closed = false;

    glm::vec3 pos = cursor.position;
    glm::vec3 start = {0, 0, 0};
    float firstStep = 0;
    glm::vec2 uvStart {0, 0};
    float uvFirstStep = 0;

    glm::vec2 uv = o1.uvFromPos(pos);
    glm::vec2 st;
    if (inters.size() == 2) {
        st = o2.uvFromPos(pos);
    } else {
        st = o2.uvFromPos(pos, uv, parametricDistance);
    }
    bool out = false;

    for (int i = 0; i < 5; i++) {
        glm::vec4 grad = gradient(o1, o2, uv, st);
        uv -= glm::vec2(grad.x, grad.y);
        st -= glm::vec2(grad.z, grad.w);
        auto t1 = o1.isOutside(uv);
        auto t2 = o2.isOutside(st);
        if (t1.x || t1.y || t2.x || t2.y) {
            out = true;
            break;
        }
    }

    if (out) return;

    for (int i = 0; i < intersectionMaxCount; i++) {
        glm::vec3 p1 = o1.evaluate(uv);
        glm::vec3 p2 = o2.evaluate(st);
        pos = (p1 + p2) / 2.f;
        positions.push_back(pos);
        uvs.push_back(uv);
        sts.push_back(st);
        if (out) break;
        if (i == 0) {
            start = pos;
            uvStart = uv;
        }
        if (i == 1) {
            firstStep = glm::length(pos - start);
            uvFirstStep = glm::length(uv - uvStart);
        }
        if (i > 1 && glm::length(pos - start) < firstStep * 1.2f && glm::length(uv - uvStart) < uvFirstStep * 1.2f) {
            closed = true;
            break;
        }
        glm::vec3 n1 = o1.evaluateNormal(uv);
        glm::vec3 n2 = o2.evaluateNormal(st);
        n1 = glm::normalize(n1);
        n2 = glm::normalize(n2);
        glm::vec3 n = glm::cross(n1, n2);
        if (glm::length(n) < normalThreshold) out = true;
        n = glm::normalize(n);
        float d = glm::dot(n, pos);
        d += interDelta;
        glm::bvec4 outside = {false, false, false, false};
        for (int j = 0; j < 3; j++) {
            glm::mat4 inv = glm::inverse(jacobi(o1, o2, uv, st, n));
            glm::vec3 p1 = o1.evaluate(uv);
            glm::vec3 p2 = o2.evaluate(st);
            float dist = glm::dot(p1 - n * d, n);
            glm::vec4 value = {p1 - p2, dist};
            glm::vec4 delta = inv * value;
            delta *= 0.02f;
            uv -= glm::vec2(delta.x, delta.y);
            st -= glm::vec2(delta.z, delta.w);
            auto a1 = o1.isOutside(uv);
            auto a2 = o2.isOutside(st);
            outside = {a1, a2};
            if (glm::any(outside)) {
                out = true;
                break;
            }
        }
    }

    if (out) {
        out = false;
        uv = uvs.front();
        st = sts.front();
        std::reverse(std::begin(positions), std::end(positions));
        std::reverse(std::begin(uvs), std::end(uvs));
        std::reverse(std::begin(sts), std::end(sts));
        for (int i = 0; i < intersectionMaxCount; i++) {
            glm::vec3 p1 = o1.evaluate(uv);
            glm::vec3 p2 = o2.evaluate(st);
            pos = (p1 + p2) / 2.f;
            if (i) {
                positions.push_back(pos);
                uvs.push_back(uv);
                sts.push_back(st);
            }
            if (out) break;
            glm::vec3 n1 = o1.evaluateNormal(uv);
            glm::vec3 n2 = o2.evaluateNormal(st);
            n1 = glm::normalize(n1);
            n2 = glm::normalize(n2);
            glm::vec3 n = glm::cross(n1, n2);
            if (glm::length(n) < normalThreshold) out = true;
            n = glm::normalize(n);
            float d = glm::dot(n, pos);
            d -= interDelta;
            glm::bvec4 outside = {false, false, false, false};
            for (int j = 0; j < 3; j++) {
                glm::mat4 inv = glm::inverse(jacobi(o1, o2, uv, st, n));
                glm::vec3 p1 = o1.evaluate(uv);
                glm::vec3 p2 = o2.evaluate(st);
                float dist = glm::dot(p1 - n * d, n);
                glm::vec4 value = {p1 - p2, dist};
                glm::vec4 delta = inv * value;
                delta *= 0.02f;
                uv -= glm::vec2(delta.x, delta.y);
                st -= glm::vec2(delta.z, delta.w);
                auto a1 = o1.isOutside(uv);
                auto a2 = o2.isOutside(st);
                outside = {a1, a2};
                if (glm::any(outside)) {
                    out = true;
                    break;
                }
            }
        }
    }

    inters.clear();
    if (positions.size() < 6) return;

    bool uvTrim = isTrimming(uvs, uvRange, closed, o1.uLoops(), o1.vLoops());
    bool stTrim = isTrimming(sts, stRange, closed, o2.uLoops(), o2.vLoops());

    if (uvTrim) {
        o1.intersected = true;
        o1.uvs.insert(o1.uvs.end(), uvs.begin(), uvs.end());
        if (closed)
            o1.uvs.push_back(uvs.front());
        o1.uvEnds.push_back(o1.uvs.size()-1);
    }

    if (stTrim) {
        o2.intersected = true;
        o2.uvs.insert(o2.uvs.end(), sts.begin(), sts.end());
        if (closed)
            o2.uvs.push_back(sts.front());
        o2.uvEnds.push_back(o2.uvs.size()-1);
    }

    auto o = Object::createIntersection();
    o.intersection->uvRange = uvRange;
    o.intersection->stRange = stRange;
    o.intersection->uvs = uvs;
    o.intersection->sts = sts;
    o.intersection->positions = positions;
    o.intersection->closed = closed;
    o.intersection->uvTrim = uvTrim;
    o.intersection->stTrim = stTrim;
    snprintf(o.intersection->uvName, OBJECT_MAX_NAME_LENGTH, "%s", o1.name);
    snprintf(o.intersection->stName, OBJECT_MAX_NAME_LENGTH, "%s", o2.name);
    objects.push_back(o);

}

void doCircle(cadence::Object** objects, float radius, glm::vec3 position, int n) {
    for (int i = 0; i < n; i++, objects++) {
        (*objects)->point->position = glm::vec3(0, radius * glm::cos(glm::pi<float>() * i * 2 / n), radius * glm::sin(glm::pi<float>() * i * 2 / n)) + position;
    }
}

void doCircle2(cadence::Object** objects, float radius, glm::vec3 position, int n) {
    for (int i = 0; i < n; i++, objects++) {
        (*objects)->point->position = glm::vec3(-radius * glm::cos(glm::pi<float>() * i * 2 / n), 0, radius * glm::sin(glm::pi<float>() * i * 2 / n)) + position;
    }
}

void cadence::App::createHeli() {
    auto o = createFlakeC2(6, 8, 1, 1, true, {0, 0, 0}, 0);
    auto it = o->flakeC2->points.data();
    float x = -3;
    doCircle(it, -1, {x, -0.6, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 0, {x, 0.2, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 1, {x, 1, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 1, {x, 1, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 1, {x, 1, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 0.3, {x, 1.6, 0}, 6);
    it += 9;
    x += 1;
    doCircle(it, 0.3, {x, 1.6, 0}, 6);
    it += 9;
    x += 2;
    doCircle(it, 0.3, {x, 1.6, 0}, 6);
    it += 9;
    x += 0.4;
    doCircle(it, 0.3, {x, 1.6, 0}, 6);
    it += 9;
    x += 0.4;
    doCircle(it, 0, {x, 1.6, 0}, 6);
    it += 9;
    x += -0.4;
    doCircle(it, -0.3, {x, 1.6, 0}, 6);

    o = createFlakeC0(2, 3, 0.15, 5, true, {0.8, 2.2, 0}, 0);
    it = o->flakeC0->points.data();
    for (int i = 0; i < 6; i++) {
        (*it)->point->position = {-1.7, 2.2, 0};
        it++;
    }
    it++;
    for (int i = 0; i < 7 * 8; i++) it++;
    for (int i = 0; i < 6; i++) {
        (*it)->point->position = {3.3, 2.2, 0};
        it++;
    }

    float r = 0.7;
    o = createFlakeC2(4, 1, 0.8, 1.3, true, {0.8, 1.98, 0}, 1);
    it = o->flakeC0->points.data();
    (*it)->point->position += glm::vec3(0, 0, r);
    it++;
    (*it)->point->position += glm::vec3(r, 0, 0);
    it++;
    (*it)->point->position -= glm::vec3(0, 0, r);
    it++;
    (*it)->point->position -= glm::vec3(r, 0, 0);
    it += 4;
    (*it)->point->position -= glm::vec3(0, 0, r);
    it++;
    (*it)->point->position -= glm::vec3(r, 0, 0);
    it++;
    (*it)->point->position += glm::vec3(0, 0, r);
    it++;
    (*it)->point->position += glm::vec3(r, 0, 0);
    it += 4;
    (*it)->point->position -= glm::vec3(0, 0, r);
    it++;
    (*it)->point->position -= glm::vec3(r, 0, 0);
    it++;
    (*it)->point->position += glm::vec3(0, 0, r);
    it++;
    (*it)->point->position += glm::vec3(r, 0, 0);

    o = createFlakeC2(4, 8, 0.2, 1.3, true, {5.4, 1.1, 0}, 1);
    it = o->flakeC0->points.data();
    doCircle(it, 0.35, {2.5, 1.6, 0}, 4);
    it += 7;
    doCircle(it, 0.35, {3, 1.6, 0}, 4);
    it += 7;
    doCircle(it, 0.35, {3.5, 1.6, 0}, 4);
    it += 7;
    doCircle(it, 0.2, {4, 1.2, 0}, 4);
    it += 7;
    doCircle(it, 0.2, {4.8, 0.7, 0}, 4);
    it += 7;
    doCircle(it, 0.2, {5, 0.7, 0}, 4);
    it += 7;
    doCircle(it, 0.2, {5.2, 0.7, 0}, 4);
    it += 7;
    doCircle2(it, 0.15, {5.6, 1, 0}, 4);
    it += 7;
    doCircle2(it, 0.1, {5.5, 1.3, 0}, 4);
    it += 7;
    doCircle2(it, 0.1, {5.4, 1.5, 0}, 4);
    it += 7;
    doCircle2(it, 0.1, {5.3, 1.6, 0}, 4);
}

cadence::Object* cadence::App::createFlakeC0(int width, int height, float w, float h, bool cylinder, glm::vec3 position, int orientation) {
    auto o = Object::createFlakeC0();
    o.flakeC0->height = height;
    o.flakeC0->width = width;
    o.flakeC0->vRes = 3;
    o.flakeC0->hRes = 3;
    o.flakeC0->cylinder = cylinder;

    size_t vCount = height * 3 + 1;
    size_t hCount = width * 3 + 1;

    objects.push_back(o);
    Object* parent = &(objects.back());

    if (cylinder) {
        for (int i = 0; i < vCount; ++i) {
            Object* seam;
            for (int j = 0; j < hCount - 1; ++j) {
                float angle = glm::pi<float>() * 2 / (hCount - 1);
                angle *= j;
                auto p = Object::createPoint({sin(angle) * w, -h/2 + h * i / (float) (vCount - 1), cos(angle) * w});
                if (orientation == 0) {
                    float temp = p.point->position.x;
                    p.point->position.x = p.point->position.y;
                    p.point->position.y = temp;
                } else if (orientation == 2) {
                    float temp = p.point->position.z;
                    p.point->position.z = p.point->position.y;
                    p.point->position.y = temp;
                }
                p.point->position += position;
                p.parents.push_back(parent);
                objects.push_back(p);
                parent->flakeC0->points.push_back(&(objects.back()));
                if (!j) seam = &(objects.back());
            }
            parent->flakeC0->points.push_back(seam);
        }
    } else {
        for (int i = 0; i < vCount; ++i) {
            for (int j = 0; j < hCount; ++j) {
                auto p = Object::createPoint({-h/2 + h * i / (float) (vCount - 1), 0, -w/2 + w * j / float (hCount - 1)});
                if (orientation == 0) {
                    float temp = p.point->position.x;
                    p.point->position.x = p.point->position.y;
                    p.point->position.y = temp;
                } else if (orientation == 2) {
                    float temp = p.point->position.z;
                    p.point->position.z = p.point->position.y;
                    p.point->position.y = temp;
                }
                p.point->position += position;
                p.parents.push_back(parent);
                objects.push_back(p);
                parent->flakeC0->points.push_back(&(objects.back()));
            }
        }
    }
    return parent;
}

cadence::Object* cadence::App::createFlakeC2(int width, int height, float w, float h, bool cylinder, glm::vec3 position, int orientation) {
    auto o = Object::createFlakeC2();
    o.flakeC2->height = height;
    o.flakeC2->width = width;
    o.flakeC2->vRes = 3;
    o.flakeC2->hRes = 3;
    o.flakeC2->cylinder = cylinder;

    size_t vCount = height + 3;
    size_t hCount = width + 3;

    objects.push_back(o);
    Object* parent = &(objects.back());

    if (cylinder) {
        for (int i = 0; i < vCount; ++i) {
            Object* seam[3];
            for (int j = 0; j < hCount - 3; ++j) {
                float angle = glm::pi<float>() * 2 / (hCount - 3);
                angle *= j;
                auto p = Object::createPoint({sin(angle) * w, -h/2 + h * i / (float) (vCount - 1), cos(angle) * w});
                if (orientation == 0) {
                    float temp = p.point->position.x;
                    p.point->position.x = p.point->position.y;
                    p.point->position.y = temp;
                } else if (orientation == 2) {
                    float temp = p.point->position.z;
                    p.point->position.z = p.point->position.y;
                    p.point->position.y = temp;
                }
                p.point->position += position;
                p.parents.push_back(parent);
                objects.push_back(p);
                parent->flakeC2->points.push_back(&(objects.back()));
                if (j < 3) seam[j] = &(objects.back());
            }
            parent->flakeC2->points.push_back(seam[0]);
            parent->flakeC2->points.push_back(seam[1]);
            parent->flakeC2->points.push_back(seam[2]);
        }
    } else {
        for (int i = 0; i < vCount; ++i) {
            for (int j = 0; j < hCount; ++j) {
                auto p = Object::createPoint({-h/2 + h * i / (float) (vCount - 1), 0, -w/2 + w * j / float (hCount - 1)});
                if (orientation == 0) {
                    float temp = p.point->position.x;
                    p.point->position.x = p.point->position.y;
                    p.point->position.y = temp;
                } else if (orientation == 2) {
                    float temp = p.point->position.z;
                    p.point->position.z = p.point->position.y;
                    p.point->position.y = temp;
                }
                p.point->position += position;
                p.parents.push_back(parent);
                objects.push_back(p);
                parent->flakeC2->points.push_back(&(objects.back()));
            }
        }
    }
    return parent;
}

void cadence::App::translateSomething(glm::vec3 translation) {
    switch (state) {
    case CAMERA:
        renderer.camera.position += translation;
        break;
    case CURSOR:
        cursor.position += translation;
        break;
    case CURSOR_MOVE:
        cursor.position += translation;
        for (auto o : selection) {
            switch (o->type) {
            case OBJECT_POINT:
                o->point->position += translation;
                if (o->parents.size() && o->parents[0]->type == OBJECT_BEZIERC2) {
                    o->parents[0]->bezierC2->virtualMoved(o);
                }
                break;
            case OBJECT_TORUS:
                o->torus->position += translation;
                break;
            case OBJECT_BEZIERC0:
            case OBJECT_BEZIERC2:
            case OBJECT_BEZIERC2I:
            case OBJECT_FLAKEC0:
            case OBJECT_FLAKEC2:
            case OBJECT_GREGORY:
            case OBJECT_INTERSECTION:
                break;
            }
        }
        break;
    case CURVE_DESIGN:
        cursor.position += translation;
        helper.point->position += translation;
        break;
    }

}

void cadence::App::handleKeyboard(float dt, glm::vec3 forward, glm::vec3 side) {
    const Uint8* state = SDL_GetKeyboardState(nullptr);

    if(state[SDL_SCANCODE_W]) {
        translateSomething(-movSpeed * forward * dt);
    }
    if(state[SDL_SCANCODE_S]) {
        translateSomething(movSpeed * forward * dt);
    }
    if(state[SDL_SCANCODE_Q]) {
        translateSomething({0, movSpeed * dt, 0});
    }
    if(state[SDL_SCANCODE_Z]) {
        translateSomething({0, -movSpeed * dt, 0});
    }
    if(state[SDL_SCANCODE_D]) {
        translateSomething(movSpeed * side * dt);
    }
    if(state[SDL_SCANCODE_A]) {
        translateSomething(movSpeed * -side * dt);
    }
    if(state[SDL_SCANCODE_J]) {
        if (App::state == CAMERA)
            renderer.camera.rotation.y += rotSpeed * dt;
    }
    if(state[SDL_SCANCODE_L]) {
        if (App::state == CAMERA)
            renderer.camera.rotation.y -= rotSpeed * dt;
    }
    if(state[SDL_SCANCODE_I]) {
        if (App::state == CAMERA)
            renderer.camera.rotation.x += rotSpeed * dt;
    }
    if(state[SDL_SCANCODE_K]) {
        if (App::state == CAMERA)
            renderer.camera.rotation.x -= rotSpeed * dt;
    }
    if(state[SDL_SCANCODE_E]) {
        if (App::state == CAMERA)
            renderer.camera.scale *= glm::pow(scaleSpeed, dt);
        if (App::state == CURSOR)
            cursorRange *= glm::pow(scaleSpeed, dt);
    }
    if(state[SDL_SCANCODE_C]) {
        if (App::state == CAMERA)
            renderer.camera.scale /= glm::pow(scaleSpeed, dt);
        if (App::state == CURSOR)
            cursorRange /= glm::pow(scaleSpeed, dt);
    }
}

void cadence::App::cameraWindow() {
    if (hideUI || !showCameraWindow)
        return;

    ImGui::Begin("renderer.camera controls", NULL, 0);
    ImGui::Checkbox("Guidelines", &renderer.guides);
    ImGui::Checkbox("Simplified curves", &renderer.simple);
    ImGui::Separator();
    ImGui::DragFloat3("renderer.camera position", &renderer.camera.position.x);
    ImGui::DragFloat2("renderer.camera rotation", &renderer.camera.rotation.x);
    ImGui::DragFloat("renderer.camera scale", &renderer.camera.scale);
    ImGui::Separator();
    ImGui::DragFloat("Field of view", &renderer.camera.fov);
    ImGui::DragFloat("Near plane", &renderer.camera.near);
    ImGui::DragFloat("Far plane", &renderer.camera.far);
    ImGui::Separator();
    ImGui::Checkbox("Stereoscopy", &stereo);
    ImGui::DragFloat("Stereo renderer.cameras separation", &renderer.camera.stereoDistance, 0.02f, 0, std::numeric_limits<float>::max());
    ImGui::DragFloat("Stereo renderer.cameras focus", &renderer.camera.stereoFocus, 0.02f, 0, std::numeric_limits<float>::max());
    ImGui::Separator();
    ImGui::DragFloat("Movement speed", &movSpeed);
    ImGui::DragFloat("Rotation speed", &rotSpeed);
    ImGui::DragFloat("Scaling speed", &scaleSpeed);
    ImGui::End();
}
