#pragma once

#include <glm/glm.hpp>

namespace cadence {

glm::mat4 identity();
glm::mat4 rotateX(float radians);
glm::mat4 rotateY(float radians);
glm::mat4 rotateZ(float radians);
glm::mat4 translate(glm::vec3 offset);
glm::mat4 scale(glm::vec3 factor);
glm::mat4 perspective(float fov, float aspect, float near, float far);
glm::mat4 frustum(float l, float r, float b, float t, float n, float f);
glm::mat4 offAxis(float fov, float aspect, float near, float far, float focus, float offset);

}
