#version 330 core

out vec3 outColor;

in vec2 uv;

uniform sampler2D renderedTexture;

void main()
{
//    outColor = vec3(0.0, 1.0, 0.0);
    outColor = texture(renderedTexture, uv).rgb;
}
