#pragma once

namespace cadence {

typedef enum {
    CAMERA,
    CURSOR,
    CURSOR_MOVE,
    CURVE_DESIGN,
} mode;

}
