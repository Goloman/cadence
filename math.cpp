#include "math.h"

glm::mat4 cadence::identity() {
    return {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
}

glm::mat4 cadence::rotateX(float radians) {
    return {
        1, 0, 0, 0,
        0, glm::cos(radians), -glm::sin(radians), 0,
        0, glm::sin(radians), glm::cos(radians), 0,
        0, 0, 0, 1
    };
}

glm::mat4 cadence::rotateY(float radians) {
    return {
        glm::cos(radians), 0, glm::sin(radians), 0,
        0, 1, 0, 0,
        -glm::sin(radians), 0, glm::cos(radians), 0,
        0, 0, 0, 1
    };
}

glm::mat4 cadence::rotateZ(float radians) {
    return {
        glm::cos(radians), -glm::sin(radians), 0, 0,
        glm::sin(radians), glm::cos(radians), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
}

glm::mat4 cadence::translate(glm::vec3 offset) {
    return {
        1, 0, 0, offset[0],
        0, 1, 0, offset[1],
        0, 0, 1, offset[2],
        0, 0, 0, 1
    };
}

glm::mat4 cadence::scale(glm::vec3 factor) {
    return {
        factor[0], 0, 0, 0,
        0, factor[1], 0, 0,
        0, 0, factor[2], 0,
        0, 0, 0, 1
    };
}

glm::mat4 cadence::perspective(float fov, float aspect, float near, float far) {
    float ctg = 1 / glm::tan(fov/2);
    return {
        ctg, 0, 0, 0,
        0, ctg*aspect, 0, 0,
        0, 0, -far/(far-near), -(far * near)/(far - near),
        0, 0, -1, 0
    };
}

glm::mat4 cadence::frustum(float l, float r, float b, float t, float n, float f) {
    return {
        2 * n / (r - l), 0, (r + l) / (r - l), 0,
        0, 2 * n / (t - b), (t + b) / (t - b), 0,
        0, 0, - (f + n) / (f - n), - 2 * f * n / (f - n),
        0, 0, -1, 0
    };
}

glm::mat4 cadence::offAxis(float fov, float aspect, float near, float far, float focus, float offset) {
    float wd2 = near * glm::tan(fov / 2);
    float ndfl = near / focus;

    float left = - aspect * wd2 - offset * ndfl;
    float right = aspect * wd2 - offset * ndfl;
    float top = wd2;
    float bottom = - wd2;

    return cadence::frustum(left, right, bottom, top, near, far);
}
