#pragma once

#include "object.h"

#include <SDL.h>
#include <GL/gl3w.h>
#include <glm/glm.hpp>
#ifndef GLM_ENABLE_EXPERIMENTAL
#define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/gtx/std_based_type.hpp>

#include <vector>

namespace cadence {

struct Camera {
    void updateMatrices();
    glm::ivec2 screenPos(glm::vec3 pos);
    glm::ivec2 screenPosUnbound(glm::vec3 pos);

    glm::vec3 position;
    glm::vec2 rotation;
    float scale;
    float fov;
    float near;
    float far;
    float stereoDistance;
    float stereoFocus;
    glm::mat4 viewMatrix;
    glm::mat4 viewMatrixL;
    glm::mat4 viewMatrixR;
    glm::mat4 projectionMatrix;
    glm::mat4 projectionMatrixL;
    glm::mat4 projectionMatrixR;
    glm::vec3 forward;
    glm::vec3 side;
};

struct SubGregory {
    glm::vec3 points[20];
    glm::mat4 matrix(float u, float v, int i);
};

struct Renderer {
    void init(bool fullscreen);
    void cleanup();

    void startFrame();
    void endFrame();

    void schedule(Object &object);
    void schedulePoint(Point &point);
    void scheduleTorus(Torus &torus, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends);
    void scheduleBezierC0(BezierC0 &bezier);
    void scheduleBezierC2(BezierC2 &bezier);
    void scheduleBezierC2I(BezierC2I &bezier);
    void scheduleFlakeC0(FlakeC0 &flake, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends);
    void scheduleFlakeC2(FlakeC2 &flake, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends);
    void scheduleGregory(Gregory &gregory);
    void scheduleIntersection(Intersection &intersection);
    void scheduleSubGregory(SubGregory &gregory, int uRes, int vRes, bool fishbones, bool normals);
    void scheduleLine(glm::vec3 p1, glm::vec3 p2);

    void scheduleBezierC0Segment(glm::vec3 *positions, size_t count);
    void scheduleBezierC0Segment(glm::vec3 *positions, size_t count, std::vector<glm::vec2> uvs, glm::vec2 range, bool even, glm::vec2 start, bool alongU, std::vector<size_t> ends);
    void scheduleCursor(Point &point, float range);
    void clearBuffers();
    void draw(glm::mat4 view, glm::mat4 perspectve, glm::vec3 color);
    void drawAdd(glm::mat4 view, glm::mat4 perspectve, glm::vec3 color);

    glm::vec3 viewed(glm::vec3 vec);

    Camera camera;

    size_t pointSize;

    std::vector<glm::vec3> points;
    std::vector<glm::vec3> linePoints;
    std::vector<unsigned int> lineIndices;

    glm::mat4 *viewMatrix;

    GLuint vao;
    GLuint pointBuffer;
    GLuint linePointBuffer;
    GLuint lineIndexBuffer;

    SDL_Window *window;
    SDL_GLContext glcontext;

    GLuint programID;
    GLuint viewMatID;
    GLuint perspectiveMatID;
    GLuint colorID;

    GLuint quadProgramID;
    GLuint framebufferID;
    GLuint textureID;
    GLuint vaoQuad;
    GLuint quadBuffer;
    GLuint textureUniformID;

    bool guides;
    bool simple;
};

}
