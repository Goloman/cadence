#include "cadence.h"

int main(int argc, char** argv)
{
    cadence::App app;
    app.fullscreen = argc <= 1;

    return app.run();
}
