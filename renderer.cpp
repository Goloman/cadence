#include "renderer.h"

#include "math.h"
#include "helpers/utils.h"
#include "helpers/imgui_impl_sdl_gl3.h"

#include <glm/gtx/transform.hpp>

float quadVertices[] = {
    1.0, -1.0,
    -1.0, -1.0,
    -1.0, 1.0,
    1.0, 1.0,
};

void cadence::Camera::updateMatrices() {
    rotation.x = glm::clamp(rotation.x, -90.0f, 90.0f);
    if (rotation.y >= 360)
        rotation.y -= 360;
    if (rotation.y < 0)
        rotation.y += 360;
    fov = glm::clamp(fov, 30.0f, 1500.0f);
    if (stereoDistance < 0)
        stereoDistance = 0;
    if (stereoFocus < 0)
        stereoFocus = 0;
    if (near < 0)
        near = 0;
    if (far < 2*near)
        far = 2*near;

    auto rot = cadence::rotateY(glm::radians(-rotation.y));
    forward = glm::vec3(rot * glm::vec4(0, 0, 1, 1));
    side = glm::vec3(rot * glm::vec4(1, 0, 0, 1));

    viewMatrix = glm::mat4(1.0f);
    viewMatrix *= cadence::scale(glm::vec3(scale));
    viewMatrix *= cadence::translate(-position);
    viewMatrix *= cadence::rotateY(glm::radians(-rotation.y));
    viewMatrix *= cadence::rotateX(glm::radians(-rotation.x));
    viewMatrixL = viewMatrix * cadence::translate(glm::vec3(stereoDistance / 2, 0, 0));
    viewMatrixR = viewMatrix * cadence::translate(glm::vec3(-stereoDistance / 2, 0, 0));

    projectionMatrix = cadence::offAxis(glm::radians(fov), (float) ImGui::GetIO().DisplaySize.x / (float)ImGui::GetIO().DisplaySize.y, near, far, -stereoFocus, 0);
    projectionMatrixL = cadence::offAxis(glm::radians(fov), (float) ImGui::GetIO().DisplaySize.x / (float)ImGui::GetIO().DisplaySize.y, near, far, -stereoFocus, stereoDistance / 2);
    projectionMatrixR = cadence::offAxis(glm::radians(fov), (float) ImGui::GetIO().DisplaySize.x / (float)ImGui::GetIO().DisplaySize.y, near, far, -stereoFocus, -stereoDistance / 2);
}

glm::ivec2 cadence::Camera::screenPos(glm::vec3 position) {
    auto io = ImGui::GetIO();
    glm::vec4 pos = glm::vec4(position, 1) * viewMatrix * projectionMatrix;
    pos /= pos.w;
    if (pos.x * pos.x > 1 || pos.y * pos.y > 1 || pos.z * pos.z > 1) return {-1, -1};
    glm::vec2 pos2 = {pos.x, pos.y};
    pos2 *= glm::vec2(0.5, -0.5);
    pos2 += glm::vec2(0.5, 0.5);
    pos2 *= glm::vec2(io.DisplaySize.x, io.DisplaySize.y);
    return pos2;
}

glm::ivec2 cadence::Camera::screenPosUnbound(glm::vec3 position) {
    auto io = ImGui::GetIO();
    glm::vec4 pos = glm::vec4(position, 1) * projectionMatrix;
    pos /= pos.w;
    glm::vec2 pos2 = {pos.x, pos.y};
    pos2 *= glm::vec2(0.5, -0.5);
    pos2 += glm::vec2(0.5, 0.5);
    pos2 *= glm::vec2(io.DisplaySize.x, io.DisplaySize.y);
    return pos2;
}

void cadence::Renderer::init(bool fullscreen) {
    glcontext = ImGui_SDLGL3_Init("cadence", fullscreen, &window);
    ImGui::CreateContext();
    ImGui_ImplSdlGL3_Init(window);
    ImGui::StyleColorsClassic();

    programID = LoadShaders("vert.glsl", "frag.glsl");
    viewMatID = glGetUniformLocation(programID, "view");
    perspectiveMatID = glGetUniformLocation(programID, "projection");
    colorID = glGetUniformLocation(programID, "color");

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &pointBuffer);
    glGenBuffers(1, &linePointBuffer);
    glGenBuffers(1, &lineIndexBuffer);

    quadProgramID = LoadShaders("quadVert.glsl", "quadFrag.glsl");
    glGenFramebuffers(1, &framebufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glGenVertexArrays(1, &vaoQuad);
    glBindVertexArray(vaoQuad);
    glGenBuffers(1, &quadBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, quadVertices, GL_STATIC_DRAW);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureID, 0);
    GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, DrawBuffers);
    textureUniformID = glGetUniformLocation(quadProgramID, "renderedTexture");
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glEnable(GL_PROGRAM_POINT_SIZE);

    simple = false;
    guides = false;
}

void cadence::Renderer::cleanup() {
    ImGui_SDLGL3_Cleanup(window, glcontext);
}

void cadence::Renderer::startFrame() {
    ImGui_ImplSdlGL3_NewFrame(window);
    glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
    static glm::vec3 clearCol = {0.0f, 0.0f, 0.0f};
    glClearColor(clearCol[0], clearCol[1], clearCol[2], 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(programID);
    glPointSize(pointSize);
}

void cadence::Renderer::endFrame() {
    ImGui::Render();
    ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
    SDL_GL_SwapWindow(window);
}

glm::vec3 cadence::Renderer::viewed(glm::vec3 vec) {
    return glm::vec3(glm::vec4(vec, 1) * *viewMatrix);
}

void cadence::Renderer::schedule(Object& object) {
    if (object.intersected && object.trimOdd && object.trimEven) return;

    switch (object.type) {
    case OBJECT_POINT:
        schedulePoint(*object.point);
        break;
    case OBJECT_TORUS:
        scheduleTorus(*object.torus, object.uvs, object.uvRange(), !object.trimOdd, !object.trimEven, object.uvEnds);
        break;
    case OBJECT_BEZIERC0:
        scheduleBezierC0(*object.bezierC0);
        break;
    case OBJECT_BEZIERC2:
        scheduleBezierC2(*object.bezierC2);
        break;
    case OBJECT_BEZIERC2I:
        scheduleBezierC2I(*object.bezierC2I);
        break;
    case OBJECT_FLAKEC0:
        scheduleFlakeC0(*object.flakeC0, object.uvs, object.uvRange(), !object.trimOdd, !object.trimEven, object.uvEnds);
        break;
    case OBJECT_FLAKEC2:
        scheduleFlakeC2(*object.flakeC2, object.uvs, object.uvRange(), !object.trimOdd, !object.trimEven, object.uvEnds);
        break;
    case OBJECT_GREGORY:
        scheduleGregory(*object.gregory);
        break;
    case OBJECT_INTERSECTION:
        scheduleIntersection(*object.intersection);
        break;
    }
}

void cadence::Renderer::scheduleIntersection(Intersection &intersection) {
    for (auto p : intersection.positions) {
        points.push_back(viewed(p));
    }

    for (int i = 0; i < intersection.positions.size() - 1; i++) {
        scheduleLine(viewed(intersection.positions[i]), viewed(intersection.positions[i+1]));
    }

    if (intersection.closed) {
        scheduleLine(viewed(intersection.positions[0]), viewed(intersection.positions[intersection.positions.size() - 1]));
    }

}

static void bezierPoint(glm::vec3 *positions, size_t count, float t) {
    for (; count > 1; --count) {
        for (size_t i = 0; i < count - 1; i++) {
            positions[i] = (1 - t) * positions[i] + t * positions[i + 1];
        }
    }
}

void cadence::Renderer::scheduleFlakeC0(FlakeC0 &flake, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends) {
    for (size_t i = 0; i < flake.height; ++i) {
        for (size_t j = 0; j < flake.width; ++j) {
            glm::vec3 positions[4][4];

            for (int u = 0; u < 4; u++) {
                for (int v = 0; v < 4; v++) {
                    positions[u][v] = viewed(flake.points[3 * j + u + (3 * flake.width + 1) * (i * 3 + v)]->point->position);
                }
            }

            if (guides || flake.drawGuides) {
                size_t offset = linePoints.size();

                for (int u = 0; u < 4; u++) {
                    for (int v = 0; v < 4; v++) {
                        linePoints.push_back(positions[u][v]);

                        if (u < 3) {
                            lineIndices.push_back(offset + 4 * u + v);
                            lineIndices.push_back(offset + 4 * (u+1) + v);
                        }

                        if (v < 3) {
                            lineIndices.push_back(offset + 4 * u + v);
                            lineIndices.push_back(offset + 4 * u + v + 1);
                        }
                    }
                }
            }

            for (int vert = 0; vert <= flake.vRes; ++vert) {
                float v = vert / (float) flake.vRes;
                glm::vec3 supports[4];
                for (int k = 0; k < 4; k++) {
                    glm::vec3 temp[4] = {
                        positions[0][k],
                        positions[1][k],
                        positions[2][k],
                        positions[3][k],
                    };
                    bezierPoint(temp, 4, v);
                    supports[k] = temp[0];
                }
                if (odd && even) {
                    scheduleBezierC0Segment(supports, 4);
                } else {
                    scheduleBezierC0Segment(supports, 4, uvs, uvRange, even, {j+v, i}, false, ends);
                }
            }

            for (int hor = 0; hor <= flake.hRes; ++hor) {
                float u = hor / (float) flake.hRes;
                glm::vec3 supports[4];
                for (int k = 0; k < 4; k++) {
                    glm::vec3 temp[4] = {
                        positions[k][0],
                        positions[k][1],
                        positions[k][2],
                        positions[k][3],
                    };
                    bezierPoint(temp, 4, u);
                    supports[k] = temp[0];
                }
                if (odd && even) {
                    scheduleBezierC0Segment(supports, 4);
                } else {
                    scheduleBezierC0Segment(supports, 4, uvs, uvRange, even, {j, i+u}, true, ends);
                }
            }

        }
    }
}

static void bezierFromDeBoor(glm::vec3 knots[4]) {
    glm::vec3 b[4];
    b[0] = knots[0] / 6.f + knots[1] * 2.f / 3.f + knots[2] / 6.f;
    b[1] = knots[1] * 2.f / 3.f + knots[2] / 3.f;
    b[2] = knots[1] / 3.f + knots[2] * 2.f / 3.f;
    b[3] = knots[1] / 6.f + knots[2] * 2.f / 3.f + knots[3] / 6.f;
    knots[0] = b[0];
    knots[1] = b[1];
    knots[2] = b[2];
    knots[3] = b[3];
}

void cadence::Renderer::scheduleFlakeC2(FlakeC2 &flake, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends) {
    for (size_t i = 0; i < flake.height; ++i) {
        for (size_t j = 0; j < flake.width; ++j) {
            glm::vec3 positions[4][4];

            for (int u = 0; u < 4; u++) {
                for (int v = 0; v < 4; v++) {
                    positions[u][v] = viewed(flake.points[j + u + (flake.width + 3) * (i + v)]->point->position);
                }
            }

            if (guides || flake.drawGuides) {
                size_t offset = linePoints.size();

                for (int u = 0; u < 4; u++) {
                    for (int v = 0; v < 4; v++) {
                        linePoints.push_back(positions[u][v]);

                        if (u < 3) {
                            lineIndices.push_back(offset + 4 * u + v);
                            lineIndices.push_back(offset + 4 * (u+1) + v);
                        }

                        if (v < 3) {
                            lineIndices.push_back(offset + 4 * u + v);
                            lineIndices.push_back(offset + 4 * u + v + 1);
                        }
                    }
                }
            }

            for (int vert = 0; vert <= flake.vRes; ++vert) {
                float v = vert / (float) flake.vRes;
                glm::vec3 supports[4];
                for (int k = 0; k < 4; k++) {
                    glm::vec3 temp[4] = {
                        positions[0][k],
                        positions[1][k],
                        positions[2][k],
                        positions[3][k],
                    };
                    bezierFromDeBoor(temp);
                    bezierPoint(temp, 4, v);
                    supports[k] = temp[0];
                }
                bezierFromDeBoor(supports);
                if (odd && even) {
                    scheduleBezierC0Segment(supports, 4);
                } else {
                    scheduleBezierC0Segment(supports, 4, uvs, uvRange, even, {j+v, i}, false, ends);
                }
            }

            for (int hor = 0; hor <= flake.hRes; ++hor) {
                float u = hor / (float) flake.hRes;
                glm::vec3 supports[4];
                for (int k = 0; k < 4; k++) {
                    glm::vec3 temp[4] = {
                        positions[k][0],
                        positions[k][1],
                        positions[k][2],
                        positions[k][3],
                    };
                    bezierFromDeBoor(temp);
                    bezierPoint(temp, 4, u);
                    supports[k] = temp[0];
                }
                bezierFromDeBoor(supports);
                if (odd && even) {
                    scheduleBezierC0Segment(supports, 4);
                } else {
                    scheduleBezierC0Segment(supports, 4, uvs, uvRange, even, {j, i+u}, true, ends);
                }
            }

        }
    }
}

void cadence::Renderer::schedulePoint(Point &point) {
    points.push_back(viewed(point.position));
}

void cadence::Renderer::scheduleLine(glm::vec3 p1, glm::vec3 p2) {
    size_t offset = linePoints.size();
    linePoints.push_back(p1);
    linePoints.push_back(p2);
    lineIndices.push_back(offset++);
    lineIndices.push_back(offset++);
}

static glm::vec4 bernstein3(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = ix * ix * ix;
    ret[1] = 3 * x * ix * ix;
    ret[2] = 3 * x * x * ix;
    ret[3] = x * x * x;
    return ret;
}

glm::mat4 cadence::SubGregory::matrix(float u, float v, int i) {
    float iu = 1-u;
    float iv = 1-v;
    glm::mat4 ret;
    float f0, f1, f2, f3;

    if (u == 0.0f) u = 0.00001f;
    if (iu == 0.0f) iu = 0.00001f;
    if (v == 0.0f) v = 0.00001f;
    if (iv == 0.0f) iv = 0.00001f;

    f0 = u * points[4][i] + v * points[7][i];
    f1 = iu * points[5][i] + v * points[8][i];
    f2 = iu * points[15][i] + iv * points[12][i];
    f3 = u * points[14][i] + iv * points[11][i];

    f0 /= u + v;
    f1 /= iu + v;
    f2 /= iu + iv;
    f3 /= u + iv;

    ret = {
        points[0][i],  points[1][i],  points[2][i],  points[3][i],
        points[6][i],  f0,            f1,            points[9][i],
        points[10][i], f3,            f2,           points[13][i],
        points[16][i], points[17][i], points[18][i], points[19][i],
    };

    return ret;
}

void cadence::Renderer::scheduleSubGregory(SubGregory &gregory, int uRes, int vRes, bool fishbones, bool normals) {
    glm::ivec2 screen[20];
    for (size_t i = 0; i < 20; ++i) {
        screen[i] = camera.screenPosUnbound(gregory.points[i]);
    }
    size_t maxX = 0;
    size_t maxY = 0;
    for (size_t i = 0; i < 20; ++i) {
        for (size_t j = i+1; j < 20; ++j) {
            auto diff = screen[i] - screen[j];
            size_t x = glm::abs(diff.x);
            size_t y = glm::abs(diff.y);
            maxX = x > maxX ? x : maxX;
            maxY = y > maxY ? y : maxY;
        }
    }

    int subdiv = maxX > maxY ? maxX : maxY;

    if (simple) subdiv = 512;

    size_t offset;

    if (uRes < 1) uRes = 1;
    if (vRes < 1) vRes = 1;

    for (int i = 0; i <= uRes; i++) {
        float u = i / (float)(uRes);
        offset = linePoints.size();
        for (int j = 0; j < subdiv; j++) {
            float v = j / (float)(subdiv - 1);
            float x = dot(bernstein3(u) * gregory.matrix(u, v, 0), bernstein3(v));
            float y = dot(bernstein3(u) * gregory.matrix(u, v, 1), bernstein3(v));
            float z = dot(bernstein3(u) * gregory.matrix(u, v, 2), bernstein3(v));
            glm::vec3 pos(x, y, z);
            linePoints.push_back(pos);
            if (j != 0) {
                lineIndices.push_back(offset + j - 1);
                lineIndices.push_back(offset + j);
            }
        }
    }

    for (int i = 0; i <= vRes; i++) {
        float v = i / (float)(vRes);
        offset = linePoints.size();
        for (int j = 0; j < subdiv; j++) {
            float u = j / (float)(subdiv - 1);
            float x = dot(bernstein3(u) * gregory.matrix(u, v, 0), bernstein3(v));
            float y = dot(bernstein3(u) * gregory.matrix(u, v, 1), bernstein3(v));
            float z = dot(bernstein3(u) * gregory.matrix(u, v, 2), bernstein3(v));
            glm::vec3 pos(x, y, z);
            linePoints.push_back(pos);
            if (j != 0) {
                lineIndices.push_back(offset + j - 1);
                lineIndices.push_back(offset + j);
            }
        }
    }

    if (fishbones) {
        scheduleLine(gregory.points[16], gregory.points[10]);
        scheduleLine(gregory.points[10], gregory.points[6]);
        scheduleLine(gregory.points[6], gregory.points[0]);
        scheduleLine(gregory.points[0], gregory.points[1]);
        scheduleLine(gregory.points[1], gregory.points[2]);
        scheduleLine(gregory.points[2], gregory.points[3]);
        scheduleLine(gregory.points[16], gregory.points[17]);
        scheduleLine(gregory.points[10], gregory.points[11]);
        scheduleLine(gregory.points[6], gregory.points[7]);
        scheduleLine(gregory.points[1], gregory.points[4]);
        scheduleLine(gregory.points[2], gregory.points[5]);
        scheduleLine(gregory.points[3], gregory.points[9]);
        scheduleLine(gregory.points[9], gregory.points[13]);
        scheduleLine(gregory.points[13], gregory.points[19]);
        scheduleLine(gregory.points[17], gregory.points[18]);
        scheduleLine(gregory.points[18], gregory.points[19]);
        scheduleLine(gregory.points[8], gregory.points[9]);
        scheduleLine(gregory.points[12], gregory.points[13]);
        scheduleLine(gregory.points[15], gregory.points[18]);
        scheduleLine(gregory.points[14], gregory.points[17]);
    };
}

glm::vec3 bezDer(glm::vec3 *p, float t) {
    glm::vec3 ret = (1 - t) * (1 - t) * (p[1] - p[0]);
    ret += 2 * (t - t * t) * (p[2] - p[1]);
    ret += t * t * (p[3] - p[2]);

    return ret;

}

glm::vec2 cramer(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
    int i = 0;
    int j = 1;
    float eps = 0.00001f;

    float det = a[i] * b[j] - a[j] * b[i];

    if (abs(det) < eps) {
        j = 2;
        det = a[i] * b[j] - a[j] * b[i];
        if (abs(det) < eps) {
            i = 1;
            det = a[i] * b[j] - a[j] * b[i];
        }
    }

    float d0 = c[i] * b[j] - c[j] * b[i];
    float d1 = a[i] * c[j] - a[j] * c[i];

    return glm::vec2(d0 / det, d1 / det);
}

glm::vec2 gauss(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
    int max = 0;
    if (a[1] > a[max]) max = 1;
    if (a[2] > a[max]) max = 2;

    bool swap = false;
    if (b[0] > a[max]) {
        max = 0;
        swap = true;
    }

    if (b[1] > swap ? b[max] : a[max]) {
        max = 1;
        swap = true;
    }

    if (b[2] > swap ? b[max] : a[max]) {
        max = 2;
        swap = true;
    }

    swap = false;
    if (swap) {
        glm::vec3 swapper = a;
        a = b;
        b = swapper;
    }

    float temp = a[0];
    a[0] = a[max];
    a[max] = temp;
    temp = b[0];
    b[0] = b[max];
    b[max] = temp;
    temp = c[0];
    c[0] = c[max];
    c[max] = temp;

    b[0] /= a[0];
    c[0] /= a[0];
    a[0] = 1;

    b[1] -= b[0] * a[1];
    c[1] -= c[0] * a[1];
    b[2] -= b[0] * a[2];
    c[2] -= c[0] * a[2];
    a[1] = 0;
    a[2] = 0;

    if (b[2] > b[1]) {
        temp = b[1];
        b[1] = b[2];
        b[2] = temp;
        temp = c[1];
        c[1] = c[2];
        c[2] = temp;
    }

    c[1] /= b[1];
    b[1] = 1;

    c[0] -= c[1] * b[0];
    c[2] -= c[1] * b[2];
    b[0] = 0;
    b[2] = 0;

    return glm::vec2(c.x, c.y);
}

float k(glm::vec2 kh0, glm::vec2 kh1, float v) {
    float k0 = kh0.x;
    float k1 = kh1.x;
    return k0 * (1 - v) + k1 * v;
}

float h(glm::vec2 kh0, glm::vec2 kh1, float v) {
    float h0 = kh0.y;
    float h1 = kh1.y;
    return h0 * (1 - v) + h1 * v;
}

glm::vec3 g(glm::vec3 g0, glm::vec3 g1, glm::vec3 g2, float t) {
    glm::vec3 ret = (1 - t) * (1 - t) * g0;
    ret += 2 * (t - t * t) * g1;
    ret += t * t * g2;

    return ret;
}

void cadence::Renderer::scheduleGregory(Gregory &gregory) {
    int count = gregory.flakes.size();

    SubGregory subs[count];

    for (int i = 0; i < count; i++) {
        int q = (i + 1) % count;
        int diff = gregory.pointIndicesNext[i] - gregory.pointIndicesLast[i];
        FlakeC0 *f = gregory.flakes[i]->flakeC0;
        diff /= 3;
        glm::vec3 temp[16];
        for (int j = 0; j < 4; j++) {
            temp[j] = viewed(f->points[gregory.pointIndicesLast[i] + diff*j]->point->position);
        }

        for (int j = 1; j < 4; j++) {
            for (int k = 0; k < 4 - j; k++) {
                temp[4 * j + k] = temp[4 * (j - 1) + k] + temp[4 * (j - 1) + k + 1];
                temp[4 * j + k] /= 2;
            }
        }

        subs[i].points[0] = temp[0];
        subs[i].points[1] = temp[4];
        subs[i].points[2] = temp[8];
        subs[i].points[3] = temp[12];
        subs[q].points[6] = temp[6];
        subs[q].points[10] = temp[9];
        subs[q].points[16] = temp[12];

        int off;
        if (diff * diff == 1) { //same row
            off = f->width * 3 + 1;
            if (gregory.pointIndicesLast[i] >= f->width * 3 + 1) {
                off *= -1;
            }
        } else {
            off = 1;
            if (gregory.pointIndicesLast[i] % (f->width * 3 + 1) != 0) {
                off *= -1;
            }
        }
        for (int j = 0; j < 4; j++) {
            temp[j] = viewed(f->points[gregory.pointIndicesLast[i] + diff*j + off]->point->position);
        }

        for (int j = 1; j < 4; j++) {
            for (int k = 0; k < 4 - j; k++) {
                temp[4 * j + k] = temp[4 * (j - 1) + k] + temp[4 * (j - 1) + k + 1];
                temp[4 * j + k] /= 2;
            }
        }

        subs[i].points[4] = subs[i].points[1] * 1.5f - temp[4] / 2.f;
        subs[i].points[5] = subs[i].points[2] * 1.5f - temp[8] / 2.f;
        subs[i].points[9] = subs[i].points[3] * 1.5f - temp[12] / 2.f;
        subs[q].points[17] = subs[q].points[16] * 1.5f - temp[12] / 2.f;
        subs[q].points[11] = subs[q].points[10] * 1.5f - temp[9] / 2.f;
        subs[q].points[7] = subs[q].points[6] * 1.5f - temp[6] / 2.f;
    }

    glm::vec3 qs[count];
    for (int i = 0; i < count; i++) {
        glm::vec3 dir = subs[i].points[9] - subs[i].points[3];
        qs[i] = subs[i].points[9] + dir * 0.5f;
    }

    glm::vec3 center = {0, 0, 0};
    for (int i = 0; i < count; i++) {
        center += qs[i] / (float)count;
    }

    for (int i = 0; i < count; i++) {
        subs[i].points[19] = center;

        glm::vec3 temp;

        temp = center + qs[i] * 2.f;
        temp /= 3;
        subs[i].points[13] = temp;
        subs[(i+1) % count].points[18] = temp;
    }

    for (int i = 0; i < count; i++) {
        int p = i-1;
        if (p < 0) p += count;
        int n = (i+1)%count;

        glm::vec3 a00 = subs[i].points[2] - subs[i].points[3];
        glm::vec3 b00 = subs[n].points[16] - subs[n].points[10];
        glm::vec3 g00 = (a00 + b00) / 2.f;

        glm::vec3 b10 = subs[i].points[18] - subs[i].points[19];
        glm::vec3 a10 = subs[n].points[19] - subs[n].points[13];
        glm::vec3 g20 = (a10 + b10) / 2.f;

        glm::vec3 a01 = subs[i].points[10] - subs[i].points[16];
        glm::vec3 b01 = subs[p].points[3] - subs[p].points[2];
        glm::vec3 g01 = (a01 + b01) / 2.f;

        glm::vec3 b11 = subs[i].points[13] - subs[i].points[19];
        glm::vec3 a11 = subs[p].points[19] - subs[p].points[18];
        glm::vec3 g21 = (a11 + b11) / 2.f;

        glm::vec3 g10 = (g00 + g20) / 2.f;
        glm::vec3 g11 = (g01 + g21) / 2.f;

        glm::vec3 c0[4] = {
            subs[i].points[3],
            subs[i].points[9],
            subs[i].points[13],
            subs[i].points[19],
        };

        glm::vec3 c1[4] = {
            subs[i].points[16],
            subs[i].points[17],
            subs[i].points[18],
            subs[i].points[19],
        };

        glm::vec3 c00 = bezDer(c0, 0);
        glm::vec3 c10 = bezDer(c0, 1);
        glm::vec3 c01 = bezDer(c1, 0);
        glm::vec3 c11 = bezDer(c1, 1);

        glm::vec2 kh00 = cramer(g00, c00, b00);
        glm::vec2 kh10 = cramer(g20, c10, b10);
        glm::vec2 kh01 = cramer(g01, c01, b01);
        glm::vec2 kh11 = cramer(g21, c11, b11);

        float t0 = 1 / 3.f;
        float t1 = 2 / 3.f;

        glm::vec3 d00 = k(kh00, kh10, t0) * g(g00, g10, g20, t0) + h(kh00, kh10, t0) * bezDer(c0, t0);
        glm::vec3 d10 = k(kh00, kh10, t1) * g(g00, g10, g20, t1) + h(kh00, kh10, t1) * bezDer(c0, t1);
        glm::vec3 d01 = k(kh01, kh11, t0) * g(g01, g11, g21, t0) + h(kh01, kh11, t0) * bezDer(c1, t0);
        glm::vec3 d11 = k(kh01, kh11, t1) * g(g01, g11, g21, t1) + h(kh01, kh11, t1) * bezDer(c1, t1);

        subs[i].points[8] = subs[i].points[9] + d00;
        subs[i].points[12] = subs[i].points[13] + d10;
        subs[i].points[14] = subs[i].points[17] + d01;
        subs[i].points[15] = subs[i].points[18] + d11;

        /*
        subs[i].points[8] = subs[i].points[5];
        subs[i].points[14] = subs[i].points[11];
        glm::vec3 w1 = subs[i].points[8] - subs[i].points[9];
        glm::vec3 w2 = subs[i].points[14] - subs[i].points[17];
        subs[i].points[12] = subs[i].points[13] + (a00 / 9.f + w1 * 4.f / 9.f + b10 * 4.f / 9.f);
        subs[i].points[15] = subs[i].points[18] + (a01 / 9.f + w2 * 4.f / 9.f + b11 * 4.f / 9.f);
        */
    }

    for (int i = 0; i < count; i++) {
        scheduleSubGregory(subs[i], gregory.hRes, gregory.vRes, gregory.drawFishbones, gregory.drawNormals);
    }
}

void cadence::Renderer::scheduleBezierC2(BezierC2 &bezier) {
    if (bezier.points.size() <= 1) return;
    if (guides || bezier.drawGuides) {
        size_t offset = linePoints.size();

        if (!bezier.bezierMode) {
            for (auto p : bezier.points) {
                linePoints.push_back(viewed(p->point->position));
            }
            for (size_t i = 0; i < bezier.points.size() - 1; ++i) {
                lineIndices.push_back(offset + i);
                lineIndices.push_back(offset + i + 1);
            }
        } else if (bezier.virtuals.size()) {
            for (auto p : bezier.virtuals) {
                linePoints.push_back(viewed(p.point->position));
            }
            for (size_t i = 0; i < bezier.virtuals.size() - 1; ++i) {
                lineIndices.push_back(offset + i);
                lineIndices.push_back(offset + i + 1);
            }
        }
    }
    if (bezier.points.size() <= 3) return;

    std::vector<glm::vec3> points;
    points.reserve(bezier.virtuals.size());

    for (size_t i = 0; i < bezier.virtuals.size(); ++i) {
        points.push_back(viewed(bezier.virtuals[i].point->position));
    }

    for (size_t i = 0; i < bezier.points.size() - 3; ++i) {
        scheduleBezierC0Segment(points.data() + 3 * i, 4);
    }
}

float sillyBspline(int i, int k, float t) {
    if (!k) {
        if (t < i || t >= i + 1) return 0;
        return 1;
    }
    return sillyBspline(i, k-1, t) * (t - i) / k + sillyBspline(i + 1, k - 1, t) * (i + k + 1 - t) / k;
}

float bspline(float *ts, int i, int k, size_t t) {
    if (ts[i] == ts[i+k+1] || !k) {
        if (t != i + (k+1)/2) return 0;
        return 1;
    }

    float ret = 0;

    if (t >= i && t < i+k) {
        float temp = ts[i+k] - ts[i];
        if (temp != 0)
            ret += bspline(ts, i, k-1, t)   * (ts[t] - ts[i]) / temp;
    }

    if (t >= i+1 && t < i+k+1) {
        float temp = ts[i+k+1] - ts[i+1];
        if (temp != 0)
            ret += bspline(ts, i+1, k-1, t) * (ts[i + k + 1] - ts[t]) / temp;
    }


    return ret;

}

void cadence::Renderer::scheduleBezierC2I(BezierC2I &bezier) {
    if (bezier.points.size() <= 2) return;

    /*
    printf("\n");
    printf("%f %f %f %f %f\n", sillyBspline(0, 3, 0),
            sillyBspline(0, 3, 1),
            sillyBspline(0, 3, 2),
            sillyBspline(0, 3, 3),
            sillyBspline(0, 3, 4));
    */
    size_t count = bezier.points.size() + 2;

    std::vector<glm::vec3> ds;
    ds.reserve(count);
    auto it = bezier.points.begin();
    ds.push_back((*it)->point->position);
    for (size_t i = 0; i < bezier.points.size(); ++i) {
        ds.push_back((*it)->point->position);
        ++it;
    }
    --it;
    ds.push_back((*it)->point->position);

    std::vector<float> ts;
    float acc = 0;

    ts.push_back(acc);
    ++acc;
    ts.push_back(acc);
    ++acc;
    ts.push_back(acc);
    ++acc;

    for (size_t i = 0; i < count - 2; ++i) {
        auto x = ds[i] - ds[i+1];
        float d2 = x.x * x.x + x.y * x.y + x.z * x.z;
        float d = fmax(sqrt(d2), 0.000001);
        acc += d;
        ts.push_back(acc);
    }

    ++acc;
    ts.push_back(acc);
    ++acc;
    ts.push_back(acc);
    ++acc;
    ts.push_back(acc);

    /*
    for (size_t i = 0; i < ts.size(); ++i) {
        printf("%f\n", ts[i]);
    }
    printf("\n");
    */

    std::vector<float> as;
    std::vector<float> bs;
    std::vector<float> cs;


    as.reserve(count);
    bs.reserve(count);
    cs.reserve(count);

    if (bezier.bad) {
        for (size_t i = 0; i < count; ++i) {
            as.push_back(sillyBspline(0, 3, 1));
            bs.push_back(sillyBspline(0, 3, 2));
            cs.push_back(sillyBspline(0, 3, 3));
        }
    } else {

        for (size_t i = 0; i < count; ++i) {
            if (i) {
                as.push_back(bspline(ts.data(), i-1, 3, i+2));
            } else {
                as.push_back(0);
            }
            bs.push_back(bspline(ts.data(), i, 3, i+2));
            if (i != count - 1) {
                cs.push_back(bspline(ts.data(), i+1, 3, i+2));
            } else {
                cs.push_back(0);
            }
        }
    }

    std::vector<float> cps(count);
    std::vector<glm::vec3> dps(count);

    cps[0] = cs[0] / bs[0];
    dps[0] = ds[0] / bs[0];

    for (size_t i = 1; i < count; ++i) {
        cps[i] = cs[i] / (bs[i] - as[i]*cps[i-1]);
        dps[i] = (ds[i] - as[i]*dps[i-1]) / (bs[i] - as[i]*cps[i-1]);
    }

    std::vector<glm::vec3> xs(count);

    xs[count - 1] = dps[count - 1];

    for (size_t i = count - 2; i < count - 1 /*unsigned underflow*/; --i) {
        xs[i] = dps[i] - cps[i]*xs[i+1];
    }

    for (size_t i = 0; i < count; ++i) {
        xs[i] = viewed(xs[i]);
    }

    std::vector<glm::vec3> beziers;
    beziers.reserve((count - 3) * 3 + 1);

    for (size_t i = 0; i < count - 2; i++) {
        glm::vec3 a, b, c;
        if (bezier.bad) {
            a = (xs[i] + 2.f * xs[i+1]) / 3.f;
            b = (xs[i+2] + 2.f * xs[i+1]) / 3.f;
            c = (a + b) / 2.f;
        } else {
            a = xs[i] * (as[i+1] + cs[i+1]) + xs[i+1] * bs[i+1];
            b = xs[i+2] * (as[i+1] + cs[i+1]) + xs[i+1] * bs[i+1];
            c = xs[i] * as[i+1] + xs[i+1] * bs[i+1] + xs[i+2] * cs[i+1];
        }

        if (i) beziers.push_back(a);
        beziers.push_back(c);
        if (i != count-3) beziers.push_back(b);
    }
    //printf("\n");

    for (size_t i = 0; i < count - 3; ++i) {
        scheduleBezierC0Segment(beziers.data() + 3 * i, 4);
    }
    if (bezier.bezierMode) {
        for (size_t i = 0; i < beziers.size(); ++i) {
            points.push_back(beziers[i]);
        }
    }

    if (guides || bezier.drawGuides) {
        size_t offset = linePoints.size();
        if (bezier.bezierMode) {
            for (auto p : beziers) {
                linePoints.push_back(p);
            }
            for (size_t i = 0; i < beziers.size() - 1; ++i) {
                lineIndices.push_back(offset + i);
                lineIndices.push_back(offset + i + 1);
            }
        } else {
            for (auto p : bezier.points) {
                linePoints.push_back(viewed(p->point->position));
            }
            for (size_t i = 0; i < bezier.points.size() - 1; ++i) {
                lineIndices.push_back(offset + i);
                lineIndices.push_back(offset + i + 1);
            }
        }
    }

}

void cadence::Renderer::scheduleBezierC0(BezierC0 &bezier) {
    if (bezier.points.size() <= 1) return;

    auto iter = bezier.points.begin();

    glm::vec3 points[4];
    points[0] = viewed((*iter)->point->position);
    size_t remaining = bezier.points.size() - 1;
    while (remaining >= 3) {
        ++iter;
        points[1] = viewed((*iter)->point->position);
        ++iter;
        points[2] = viewed((*iter)->point->position);
        ++iter;
        points[3] = viewed((*iter)->point->position);
        scheduleBezierC0Segment(points, 4);
        points[0] = points[3];
        remaining -= 3;
    }
    if (remaining) {
        ++iter;
        points[1] = viewed((*iter)->point->position);
        if (remaining > 1) {
            ++iter;
            points[2] = viewed((*iter)->point->position);
        }
        scheduleBezierC0Segment(points, remaining + 1);
    }


    if (!guides && !bezier.drawGuides) return;

    size_t offset = linePoints.size();
    for (auto p : bezier.points) {
        linePoints.push_back(viewed(p->point->position));
    }

    for (size_t i = 0; i < bezier.points.size() - 1; ++i) {
        lineIndices.push_back(offset + i);
        lineIndices.push_back(offset + i + 1);
    }

}

void cadence::Renderer::scheduleBezierC0Segment(glm::vec3 *positions, size_t count) {
    glm::ivec2 screen[count];
    for (size_t i = 0; i < count; ++i) {
        screen[i] = camera.screenPosUnbound(positions[i]);
    }
    size_t maxX = 0;
    size_t maxY = 0;
    for (size_t i = 0; i < count; ++i) {
        for (size_t j = i+1; j < count; ++j) {
            auto diff = screen[i] - screen[j];
            size_t x = glm::abs(diff.x);
            size_t y = glm::abs(diff.y);
            maxX = x > maxX ? x : maxX;
            maxY = y > maxY ? y : maxY;
        }
    }
    size_t subsegments = (maxX + maxY) * 2 + 1;
    if (simple) subsegments = 64;
    size_t offset = linePoints.size();
    for (size_t i = 0; i < subsegments; i++) {
        float t = 1.0 / (subsegments - 1) * i;
        glm::vec3 pos[count];
        for (size_t j = 0; j < count; j++) {
            pos[j] = positions[j];
        }
        bezierPoint(pos, count, t);
        linePoints.push_back(pos[0]);
    }
    for (size_t i = 0; i < subsegments - 1; i++) {
        lineIndices.push_back(offset + i);
        lineIndices.push_back(offset + i + 1);
    }
}

static float cross2d(glm::vec2 lhs, glm::vec2 rhs) {
    return lhs.x * rhs.y - lhs.y * rhs.x;
}

static bool intersectSegments(glm::vec2 p, glm::vec2 p1, glm::vec2 q, glm::vec2 q1, float *out) {
    glm::vec2 r = p1 - p;
    glm::vec2 s = q1 - q;
    float d = cross2d(r, s);
    float t = cross2d(q - p, s / d);
    float u = cross2d(q - p, r / d);
    if (out) *out = t;
    return (t >= 0 && t < 1 && u >= 0 && u < 1);
}

static bool isOddIntersection(glm::vec2 p, glm::vec2 range, std::vector<glm::vec2> uvs, std::vector<size_t> ends) {
    int acc = 0;
    p /= range;
    for (int i = 0; i < uvs.size() - 1; i++) {
        bool flag = false;
        for (auto j : ends) {
            if (i != j) continue;
            flag = true;
            break;
        }
        if (flag) continue;
        auto p0 = uvs[i];
        auto p1 = uvs[i + 1];
        p0 /= range;
        p1 /= range;
        bool uDiff = abs(p0.x - p1.x) > 0.5;
        bool vDiff = abs(p0.y - p1.y) > 0.5;
        if (uDiff || vDiff) {
            glm::vec2 p00 = p1;
            glm::vec2 p11 = p0;
            if (uDiff) {
                if (p0.x > 0.5) {
                    p00.x += 1;
                    p11.x -= 1;
                } else {
                    p00.x -= 1;
                    p11.x += 1;
                }
            }
            if (vDiff) {
                if (p0.y > 0.5) {
                    p00.y += 1;
                    p11.y -= 1;
                } else {
                    p00.y -= 1;
                    p11.y += 1;
                }
            }
            if (intersectSegments({0, 0}, p, p0, p00, nullptr)) acc++;
            if (intersectSegments({0, 0}, p, p1, p11, nullptr)) acc++;
        } else {
            if (intersectSegments({0, 0}, p, p0, p1, nullptr)) acc++;
        }
    }

    return (acc % 2) == 1;
}

static bool findIntersection(glm::vec2 q0, glm::vec2 q1, glm::vec2 range, std::vector<glm::vec2> uvs, float *out, std::vector<size_t> ends) {
    q0 /= range;
    q1 /= range;
    for (int i = 0; i < uvs.size() - 1; i++) {
        bool flag = false;
        for (auto j : ends) {
            if (i != j) continue;
            flag = true;
            break;
        }
        if (flag) continue;
        auto p0 = uvs[i];
        auto p1 = uvs[i + 1];
        p0 /= range;
        p1 /= range;
        bool uDiff = abs(p0.x - p1.x) > 0.5;
        bool vDiff = abs(p0.y - p1.y) > 0.5;
        if (uDiff || vDiff) {
            glm::vec2 p00 = p1;
            glm::vec2 p11 = p0;
            if (uDiff) {
                if (p0.x > 0.5) {
                    p00.x += 1;
                    p11.x -= 1;
                } else {
                    p00.x -= 1;
                    p11.x += 1;
                }
            }
            if (vDiff) {
                if (p0.y > 0.5) {
                    p00.y += 1;
                    p11.y -= 1;
                } else {
                    p00.y -= 1;
                    p11.y += 1;
                }
            }
            if (intersectSegments(q0, q1, p0, p00, out)) return true;
            if (intersectSegments(q0, q1, p1, p11, out)) return true;
        } else {
            if (intersectSegments(q0, q1, p0, p1, out)) return true;
        }
    }

    return false;
}

void cadence::Renderer::scheduleBezierC0Segment(glm::vec3 *positions, size_t count, std::vector<glm::vec2> uvs, glm::vec2 range, bool even, glm::vec2 start, bool alongU, std::vector<size_t> ends) {
    size_t subsegments = 128;
    if (simple) subsegments = 32;
    for (size_t i = 0; i < subsegments - 1; i++) {
        float t0 = 1.0 / (subsegments - 1) * i;
        float t1 = 1.0 / (subsegments - 1) * (i + 1);
        glm::vec2 uv0 = start;
        glm::vec2 uv1 = start;
        if (alongU) {
            uv0.x += t0;
            uv1.x += t1;
        } else {
            uv0.y += t0;
            uv1.y += t1;
        }

        bool b0 = isOddIntersection(uv0, range, uvs, ends);
        bool b1 = isOddIntersection(uv1, range, uvs, ends);

        if (b0 != even || b1 != even) {
            float extra;
            if (b0 != b1 && findIntersection(uv0, uv1, range, uvs, &extra, ends)) {
                if (b0 == even) {
                    glm::vec3 p0[count];
                    glm::vec3 p[count];
                    for (size_t j = 0; j < count; j++) {
                        p0[j] = positions[j];
                        p[j] = positions[j];
                    }
                    float tt = t0 + extra * (t1 - t0);
                    bezierPoint(p0, count, t0);
                    bezierPoint(p, count, tt);
                    scheduleLine(p0[0], p[0]);
                } else {
                    glm::vec3 p1[count];
                    glm::vec3 p[count];
                    for (size_t j = 0; j < count; j++) {
                        p1[j] = positions[j];
                        p[j] = positions[j];
                    }
                    float tt = t0 + extra * (t1 - t0);
                    bezierPoint(p1, count, t1);
                    bezierPoint(p, count, tt);
                    scheduleLine(p1[0], p[0]);
                }
            }
            continue;
        }

        glm::vec3 pos0[count];
        glm::vec3 pos1[count];
        for (size_t j = 0; j < count; j++) {
            pos0[j] = positions[j];
            pos1[j] = positions[j];
        }
        bezierPoint(pos0, count, t0);
        bezierPoint(pos1, count, t1);
        scheduleLine(pos0[0], pos1[0]);
    }
}

void cadence::Renderer::scheduleCursor(Point &point, float range) {
/*
    float test[] = {-4, -3, -2, -1, 0, 0, 0, 0, 0, 1, 2, 3, 4};
    for (size_t i = 2; i < 11; i++) {
        printf("%8f %8f %8f %8f %8f %8f %8f %8f %8f %3zu\n",
                bspline(test, 0, 3, i),
                bspline(test, 1, 3, i),
                bspline(test, 2, 3, i),
                bspline(test, 3, 3, i),
                bspline(test, 4, 3, i),
                bspline(test, 5, 3, i),
                bspline(test, 6, 3, i),
                bspline(test, 7, 3, i),
                bspline(test, 8, 3, i),
                i
                );
    }
    printf("\n");
*/

/*
    float test[] = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1};
    for (size_t n = 0; n <= 3; n++) {
        for (size_t start = 0; start <= 2 + n; start++) {
            for (size_t i = 0; i < 2 + n; i++) {
                printf("%f %f\n",test[3 - n + start + i], bspline(test, 3 - n + start, n, 3 - n + start + i));
            }
    printf("\n");
        }
    printf("\n");
    }
    printf("\n");
 */

    size_t offset = linePoints.size();

    linePoints.push_back(viewed(point.position + glm::vec3(range, 0, 0)));
    linePoints.push_back(viewed(point.position + glm::vec3(-range, 0, 0)));
    linePoints.push_back(viewed(point.position + glm::vec3(0, range, 0)));
    linePoints.push_back(viewed(point.position + glm::vec3(0, -range, 0)));
    linePoints.push_back(viewed(point.position + glm::vec3(0, 0, range)));
    linePoints.push_back(viewed(point.position + glm::vec3(0, 0, -range)));

    lineIndices.push_back(offset + 0);
    lineIndices.push_back(offset + 1);
    lineIndices.push_back(offset + 2);
    lineIndices.push_back(offset + 3);
    lineIndices.push_back(offset + 4);
    lineIndices.push_back(offset + 5);
}

void cadence::Renderer::clearBuffers() {
    points.clear();
    linePoints.clear();
    lineIndices.clear();
}

void cadence::Renderer::draw(glm::mat4 view, glm::mat4 perspective, glm::vec3 color) {
    glUniformMatrix4fv(viewMatID, 1, GL_TRUE, &view[0][0]);
    glUniformMatrix4fv(perspectiveMatID, 1, GL_TRUE, &perspective[0][0]);
    glUniform3fv(colorID, 1, &color[0]);

    glEnableVertexAttribArray(0);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, pointBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * points.size(), points.data(), GL_STREAM_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_POINTS, 0, points.size());

    glBindBuffer(GL_ARRAY_BUFFER, linePointBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * linePoints.size(), linePoints.data(), GL_STREAM_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, lineIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * lineIndices.size(), lineIndices.data(), GL_STREAM_DRAW);
    glDrawElements(GL_LINES, lineIndices.size(), GL_UNSIGNED_INT, (void*)0);

    glDisableVertexAttribArray(0);
}

void cadence::Renderer::drawAdd(glm::mat4 view, glm::mat4 perspective, glm::vec3 color) {
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    draw(view, perspective, color);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
    glUseProgram(quadProgramID);
    glUniform1i(textureUniformID, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glBindSampler(0, 0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(vaoQuad);
    glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDisableVertexAttribArray(0);


    glDisable(GL_BLEND);
}

void cadence::Renderer::scheduleTorus(Torus &torus, std::vector<glm::vec2> uvs, glm::vec2 uvRange, bool odd, bool even, std::vector<size_t> ends) {
    glm::mat4 world = cadence::scale(torus.scale);
    world *= cadence::rotateX(torus.rotation.x);
    world *= cadence::rotateY(torus.rotation.y);
    world *= cadence::rotateZ(torus.rotation.z);
    world *= cadence::translate(torus.position);

    for (int i = 0; i < torus.majRes; i++) {
        float u = glm::pi<float>() * 2 / torus.majRes * i;
        for (int j = 0; j < torus.minRes; j++) {
            float v0 = glm::pi<float>() * 2 / torus.minRes * j;
            float v1 = glm::pi<float>() * 2 / torus.minRes * (j+1);
            if (odd != even) {
                bool b0 = isOddIntersection({u, v0}, uvRange, uvs, ends);
                bool b1 = isOddIntersection({u, v1}, uvRange, uvs, ends);
                if (b0 != b1) {
                    float t;
                    if (findIntersection({u, v0}, {u, v1}, uvRange, uvs, &t, ends)) {
                        float step = glm::pi<float>() * 2 / torus.minRes;
                        float v = v0 + t * step;
                        if (b0 != odd) {
                            v0 = v;
                        } else {
                            v1 = v;
                        }
                    } else {
                        continue;
                    }
                } else {
                    if (b0 != odd) continue;
                }
            }
            float x0 = (torus.major + torus.minor * cos(u)) * cos(v0);
            float y0 = torus.minor * sin(u);
            float z0 = (torus.major + torus.minor * cos(u)) * sin(v0);
            glm::vec3 p0 = {x0, y0, z0};
            p0 = glm::vec3(glm::vec4(p0, 1) * world);
            float x1 = (torus.major + torus.minor * cos(u)) * cos(v1);
            float y1 = torus.minor * sin(u);
            float z1 = (torus.major + torus.minor * cos(u)) * sin(v1);
            glm::vec3 p1 = {x1, y1, z1};
            p1 = glm::vec3(glm::vec4(p1, 1) * world);
            scheduleLine(viewed(p0), viewed(p1));
        }
    }

    for (int i = 0; i < torus.minRes; i++) {
        float v = glm::pi<float>() * 2 / torus.minRes * i;
        for (int j = 0; j < torus.majRes; j++) {
            float u0 = glm::pi<float>() * 2 / torus.majRes * j;
            float u1 = glm::pi<float>() * 2 / torus.majRes * (j+1);
            if (odd != even) {
                bool b0 = isOddIntersection({u0, v}, uvRange, uvs, ends);
                bool b1 = isOddIntersection({u1, v}, uvRange, uvs, ends);
                if (b0 != b1) {
                    float t;
                    if (findIntersection({u0, v}, {u1, v}, uvRange, uvs, &t, ends)) {
                        float step = glm::pi<float>() * 2 / torus.majRes;
                        float u = u0 + t * step;
                        if (b0 != odd) {
                            u0 = u;
                        } else {
                            u1 = u;
                        }
                    } else {
                        continue;
                    }
                } else {
                    if (b0 != odd) continue;
                }
            }
            float x0 = (torus.major + torus.minor * cos(u0)) * cos(v);
            float y0 = torus.minor * sin(u0);
            float z0 = (torus.major + torus.minor * cos(u0)) * sin(v);
            glm::vec3 p0 = {x0, y0, z0};
            p0 = glm::vec3(glm::vec4(p0, 1) * world);
            float x1 = (torus.major + torus.minor * cos(u1)) * cos(v);
            float y1 = torus.minor * sin(u1);
            float z1 = (torus.major + torus.minor * cos(u1)) * sin(v);
            glm::vec3 p1 = {x1, y1, z1};
            p1 = glm::vec3(glm::vec4(p1, 1) * world);
            scheduleLine(viewed(p0), viewed(p1));
        }
    }

}

