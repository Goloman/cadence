#pragma once

#include "constants.h"
#include "object.h"
#include "renderer.h"

#include <SDL.h>
#include <imgui.h>
#include <GL/gl3w.h>
#ifndef GLM_ENABLE_EXPERIMENTAL
#define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/gtx/std_based_type.hpp>

#include <list>

namespace cadence {

class App {
public:
    int run();
    bool fullscreen;

private:
    void init();
    void loop();
    void cleanup();

    void mainMenuBar();
    void cameraWindow();
    void creationWindow();
    void listWindow();
    void diagnosticWindow();

    void createHeli();
    Object* createFlakeC2(int width, int height, float w, float h, bool cylinder, glm::vec3 position, int orientation);
    Object* createFlakeC0(int width, int height, float w, float h, bool cylinder, glm::vec3 position, int orientation);
    void createIntersection();

    void scheduleObjects();

    void loadFile();
    void saveFile();
    void clearScene();

    bool eventLoop();

    void handleKeyboard(float dt, glm::vec3 forward, glm::vec3 side);
    void translateSomething(glm::vec3 translation);
    void selectObject(Object* o);

    mode state;

    std::list<Object> objects;
    std::list<Object*> selection;
    std::list<Object*> virtuals;
    std::vector<Object*> gregPrep;
    Point cursor;
    Object helper;

    std::vector<Object*> inters;
    float interDelta = 1.f;
    int intersectionMaxCount = 1024;
    bool intersectionPreview = false;
    int previewCount = 32;
    float parametricDistance = 0.1f;
    float normalThreshold = 0.001f;

    float movSpeed;
    float rotSpeed;
    float scaleSpeed;
    float pointSize;

    float cursorRange;

    bool done;
    bool hideUI;
    bool stereo;

    char filename[512];

    Renderer renderer;
};

}
