#pragma once

#include "constants.h"

#include <glm/glm.hpp>
#include <list>
#include <vector>

#define OBJECT_MAX_NAME_LENGTH 32

namespace cadence {

struct Object;

enum Object_type {
    OBJECT_POINT,
    OBJECT_TORUS,
    OBJECT_BEZIERC0,
    OBJECT_BEZIERC2,
    OBJECT_BEZIERC2I,
    OBJECT_FLAKEC0,
    OBJECT_FLAKEC2,
    OBJECT_GREGORY,
    OBJECT_INTERSECTION,
};

struct Point {
    glm::vec3 position;
};

struct Torus {
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
    float major;
    float minor;
    int majRes;
    int minRes;
};

struct BezierC0 {
    std::list<Object*> points;
    bool drawGuides = false;
};

struct BezierC2 {
    std::list<Object*> points;
    std::vector<Object> virtuals;
    bool bezierMode = false;
    bool drawGuides = false;
    void virtualMoved(Object* point);
};

struct BezierC2I {
    std::list<Object*> points;
    bool drawGuides = false;
    bool bezierMode = false;
    bool bad = false;
};

struct FlakeC0 {
    std::vector<Object*> points;
    size_t width;
    size_t height;
    size_t vRes;
    size_t hRes;
    bool cylinder = false;
    bool drawGuides = false;
};

struct FlakeC2 {
    std::vector<Object*> points;
    size_t width;
    size_t height;
    size_t vRes;
    size_t hRes;
    bool cylinder = false;
    bool drawGuides = false;
};

struct Gregory {
    std::vector<Object*> flakes;
    std::vector<size_t> pointIndicesLast;
    std::vector<size_t> pointIndicesNext;
    size_t vRes;
    size_t hRes;
    bool drawNormals = false;
    bool drawFishbones = false;
};

struct Intersection {
    glm::vec2 uvRange;
    glm::vec2 stRange;
    std::vector<glm::vec2> uvs;
    std::vector<glm::vec2> sts;
    std::vector<glm::vec3> positions;
    bool uvTrim;
    bool stTrim;
    char uvName[OBJECT_MAX_NAME_LENGTH];
    char stName[OBJECT_MAX_NAME_LENGTH];
    bool closed;
};

struct Object {
    std::vector<Object *> parents;
    union {
        Point* point;
        Torus* torus;
        BezierC0* bezierC0;
        BezierC2* bezierC2;
        BezierC2I* bezierC2I;
        FlakeC0* flakeC0;
        FlakeC2* flakeC2;
        Gregory* gregory;
        Intersection* intersection;
    };
    Object_type type;
    bool remove;
    bool virt;
    char name[OBJECT_MAX_NAME_LENGTH];

    bool intersected = false;
    std::vector<glm::vec2> uvs;
    std::vector<size_t> uvEnds;
    bool trimOdd = false;
    bool trimEven = false;


    Object();

    void replaceWith(Object *o, Object *n);

    static void deleteObject(Object o);
    static Object createPoint(glm::vec3 position, bool fake=false);
    static Object createTorus(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, float minor, float major, int minRes, float majRes);
    static Object createBezierC0();
    static Object createBezierC2();
    static Object createBezierC2I();
    static Object createFlakeC0();
    static Object createFlakeC2();
    static Object createGregory();
    static Object createIntersection();

    static void drawControls(Object &o, mode state);
    static void update(Object &o);
    static void cleanup(Object &o);

    bool isSurface();
    bool uLoops();
    bool vLoops();
    glm::vec2 uvRange();
    glm::vec3 evaluate(glm::vec2 uv);
    glm::vec3 evaluateDu(glm::vec2 uv);
    glm::vec3 evaluateDv(glm::vec2 uv);
    glm::vec3 evaluateNormal(glm::vec2 uv);
    glm::vec2 uvFromPos(glm::vec3 pos, glm::vec2 uv = {-1, -1}, float dist = 0);
    glm::bvec2 isOutside(glm::vec2 &uv);
};

glm::vec4 gradient(Object &o1, Object &o2, glm::vec2 uv, glm::vec2 st);
glm::mat4 jacobi(Object &o1, Object &o2, glm::vec2 uv, glm::vec2 st, glm::vec3 planeNormal);

}
