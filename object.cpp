#include "object.h"
#include "math.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <imgui.h>
#include <climits>
#include <cfloat>

#include <stdio.h>

cadence::Object::Object() {
    remove = false;
    virt = false;
    name[0] = 0;
}

void cadence::Object::replaceWith(cadence::Object *o, cadence::Object *n) {
    switch(type) {
        case OBJECT_POINT:
        case OBJECT_TORUS:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
        break;
        case OBJECT_BEZIERC0:
        for (auto &p : bezierC0->points)
            if (p == o) p = n;
        break;
        case OBJECT_BEZIERC2:
        for (auto &p : bezierC2->points)
            if (p == o) p = n;
        break;
        case OBJECT_BEZIERC2I:
        for (auto &p : bezierC2I->points)
            if (p == o) p = n;
        break;
        case OBJECT_FLAKEC0:
        for (auto &p : flakeC0->points)
            if (p == o) p = n;
        break;
        case OBJECT_FLAKEC2:
        for (auto &p : flakeC2->points)
            if (p == o) p = n;
        break;
    }
}

void cadence::BezierC2::virtualMoved(Object *point) {
    size_t index = 0;
    bool found = false;

    for (size_t i = 0; i < virtuals.size(); ++i) {
        if (&virtuals[i] != point) continue;

        index = i;
        found = true;
    }

    if (!found) return;

    size_t dbIndex = (index + 4) / 3;
    size_t choice = index % 3;

    glm::vec3 root;
    glm::vec3 prev;
    glm::vec3 next;

    auto it = points.begin();
    for (size_t i = 0; i <= dbIndex + 1; ++i, ++it) {
        if (i == dbIndex-1) prev = (*it)->point->position;
        if (i == dbIndex+1) next = (*it)->point->position;
    }

    --it;
    --it;

    switch (choice) {
    case 0:
        root = (next + prev) / 2.0f;
        break;
    case 1:
        root = next;
        break;
    case 2:
        root = prev;
        break;
    default:
        return;
    }

    (*it)->point->position = point->point->position + (point->point->position - root) / 2.0f;
}

cadence::Object cadence::Object::createPoint(glm::vec3 position, bool fake) {
    static size_t counter = 0;
    Object o;
    o.virt = fake;
    o.type = OBJECT_POINT;
    if (fake) {
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "point v");
    } else {
        snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "point %zu", counter++);
    }
    o.point = new Point;
    o.point->position = position;
    return o;
}

cadence::Object cadence::Object::createBezierC0() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_BEZIERC0;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "bezier c0 %zu", counter++);
    o.bezierC0 = new BezierC0;
    return o;
}

cadence::Object cadence::Object::createBezierC2() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_BEZIERC2;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "bezier c2 %zu", counter++);
    o.bezierC2 = new BezierC2;
    return o;
}

cadence::Object cadence::Object::createBezierC2I() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_BEZIERC2I;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "bezier c2i %zu", counter++);
    o.bezierC2I = new BezierC2I;
    return o;
}

cadence::Object cadence::Object::createFlakeC0() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_FLAKEC0;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "flake c0 %zu", counter++);
    o.flakeC0 = new FlakeC0;
    return o;
}

cadence::Object cadence::Object::createFlakeC2() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_FLAKEC2;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "flake c2 %zu", counter++);
    o.flakeC2 = new FlakeC2;
    return o;
}

cadence::Object cadence::Object::createGregory() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_GREGORY;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "gregory %zu", counter++);
    o.gregory = new Gregory;
    return o;
}

cadence::Object cadence::Object::createIntersection() {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_INTERSECTION;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "intersection %zu", counter++);
    o.intersection = new Intersection;
    return o;
}

cadence::Object cadence::Object::createTorus(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, float minor, float major, int minRes, float majRes) {
    static size_t counter = 0;
    Object o;
    o.type = OBJECT_TORUS;
    snprintf(o.name, OBJECT_MAX_NAME_LENGTH, "torus c0 %zu", counter++);
    o.torus = new Torus;
    o.torus->position = position;
    o.torus->rotation = rotation;
    o.torus->scale = scale;
    o.torus->minor = minor;
    o.torus->major = major;
    o.torus->minRes = minRes;
    o.torus->majRes = majRes;
    return o;
}

void cadence::Object::deleteObject(Object o) {
    switch (o.type) {
    case OBJECT_POINT:
        delete o.point;
        break;
    case OBJECT_TORUS:
        delete o.torus;
        break;
    case OBJECT_BEZIERC0:
        delete o.bezierC0;
        break;
    case OBJECT_BEZIERC2:
        for (size_t i = 0; i < o.bezierC2->virtuals.size(); i++)
            deleteObject(o.bezierC2->virtuals[i]);
        delete o.bezierC2;
        break;
    case OBJECT_BEZIERC2I:
        delete o.bezierC2I;
        break;
    case OBJECT_FLAKEC0:
        delete o.flakeC0;
        break;
    case OBJECT_FLAKEC2:
        delete o.flakeC0;
        break;
    case OBJECT_GREGORY:
        delete o.gregory;
        break;
    case OBJECT_INTERSECTION:
        delete o.intersection;
        break;
    }
}

void cadence::Object::drawControls(Object &o, mode state) {
    ImGui::InputText("Name", o.name, OBJECT_MAX_NAME_LENGTH);
    ImGui::Separator();

    if (o.intersected) {
        ImGui::Checkbox("Trim odd", &o.trimOdd);
        ImGui::Checkbox("Trim even", &o.trimEven);
        if (ImGui::Button("Delete intersection")) {
            o.intersected = false;
            o.trimOdd = false;
            o.trimEven = false;
            o.uvs.clear();
            o.uvEnds.clear();
        }
        ImGui::Separator();
    }

    switch (o.type) {
    case OBJECT_POINT:
        {
            ImGui::DragFloat3("", &o.point->position.x, 0.02f);
        }
        break;
    case OBJECT_TORUS:
        {
            ImGui::DragFloat3("Position", &o.torus->position.x, 0.02f);
            ImGui::DragFloat3("Rotation", &o.torus->rotation.x, 0.02f);
            ImGui::DragFloat3("Scale", &o.torus->scale.x, 0.02f);
            ImGui::DragFloat("Major", &o.torus->major);
            ImGui::DragFloat("Minor", &o.torus->minor);
            ImGui::DragInt("Major resolution", &o.torus->majRes);
            ImGui::DragInt("Minor resolution", &o.torus->minRes);

        }
        break;
    case OBJECT_BEZIERC0:
        {
            auto it = o.bezierC0->points.begin();
            size_t counter = 0;
            while (it != o.bezierC0->points.end()) {
                if (state == CURVE_DESIGN && counter == o.bezierC0->points.size() - 1) {
                    ++it;
                    continue;
                }
                ImGui::PushID(*it);
                ImGui::Text("%s", (*it)->name);
                ImGui::SameLine();
                char buf[64];
                snprintf(buf, 64, "Delete##%zu", counter++);
                if (ImGui::Button(buf)) {
                   it = o.bezierC0->points.erase(it);
                } else {
                    ++it;
                }
                ImGui::PopID();
            }
            ImGui::Checkbox("Draw guides", &(o.bezierC0->drawGuides));
        }
        break;
    case OBJECT_BEZIERC2:
        {
            auto it = o.bezierC2->points.begin();
            size_t counter = 0;
            while (it != o.bezierC2->points.end()) {
                if (state == CURVE_DESIGN && counter == o.bezierC2->points.size() - 1) {
                    ++it;
                    continue;
                }
                ImGui::PushID(*it);
                ImGui::Text("%s", (*it)->name);
                ImGui::SameLine();
                char buf[64];
                snprintf(buf, 64, "Delete##%zu", counter++);
                if (ImGui::Button(buf)) {
                   it = o.bezierC2->points.erase(it);
                } else {
                    ++it;
                }
                ImGui::PopID();
            }
            ImGui::Checkbox("Draw guides", &(o.bezierC2->drawGuides));
            ImGui::Checkbox("Bezier mode", &(o.bezierC2->bezierMode));
        }
        break;
    case OBJECT_BEZIERC2I:
        {
            auto it = o.bezierC2I->points.begin();
            size_t counter = 0;
            while (it != o.bezierC2I->points.end()) {
                if (state == CURVE_DESIGN && counter == o.bezierC2I->points.size() - 1) {
                    ++it;
                    continue;
                }
                ImGui::PushID(*it);
                ImGui::Text("%s", (*it)->name);
                ImGui::SameLine();
                char buf[64];
                snprintf(buf, 64, "Delete##%zu", counter++);
                if (ImGui::Button(buf)) {
                    it = o.bezierC2I->points.erase(it);
                } else {
                    ++it;
                }
                ImGui::PopID();
            }
            ImGui::Checkbox("Draw guides", &(o.bezierC2I->drawGuides));
            ImGui::Checkbox("Show bezier", &(o.bezierC2I->bezierMode));
            ImGui::Checkbox("Simplified curve", &(o.bezierC2I->bad));
        }
        break;
    case OBJECT_FLAKEC0:
        {

            ImGui::InputInt("Vertical resolution", (int*)&o.flakeC0->vRes);
            ImGui::InputInt("Horizontal resolution", (int*)&o.flakeC0->hRes);

            if (!(o.flakeC0->vRes)) o.flakeC0->vRes = 1;
            if (!(o.flakeC0->hRes)) o.flakeC0->hRes = 1;

            ImGui::Checkbox("Draw guides", &(o.flakeC0->drawGuides));

            ImGui::Separator();

            for(auto &p : o.flakeC0->points) {
                ImGui::Text("%s", p->name);
            }
        }
        break;
    case OBJECT_FLAKEC2:
        {

            ImGui::InputInt("Vertical resolution", (int*)&o.flakeC2->vRes);
            ImGui::InputInt("Horizontal resolution", (int*)&o.flakeC2->hRes);

            if (!(o.flakeC2->vRes)) o.flakeC2->vRes = 1;
            if (!(o.flakeC2->hRes)) o.flakeC2->hRes = 1;

            ImGui::Checkbox("Draw guides", &(o.flakeC2->drawGuides));

            ImGui::Separator();

            for(auto &p : o.flakeC2->points) {
                ImGui::Text("%s", p->name);
            }
        }
        break;
    case OBJECT_GREGORY:
        {
            ImGui::InputInt("Vertical resolution", (int*)&o.gregory->vRes);
            ImGui::InputInt("Horizontal resolution", (int*)&o.gregory->hRes);

            if (!(o.gregory->vRes)) o.gregory->vRes = 1;
            if (!(o.gregory->hRes)) o.gregory->hRes = 1;

            //ImGui::Checkbox("Draw normals", &(o.gregory->drawNormals));
            ImGui::Checkbox("Draw fishbones", &(o.gregory->drawFishbones));
        }
        break;
    case OBJECT_INTERSECTION:
        {
            float size = 200.f;
            float s2 = size/2;

            ImGui::Text("%s", o.intersection->uvName);
            ImDrawList *draw_list = ImGui::GetWindowDrawList();
            ImVec2 canvas_pos = ImGui::GetCursorScreenPos();            // ImDrawList API uses screen coordinates!
            ImVec2 canvas_size;
            canvas_size.x = size;
            canvas_size.y = size;
            ImGui::InvisibleButton("canvas", canvas_size);

            draw_list->AddRectFilledMultiColor(canvas_pos, ImVec2(canvas_pos.x + canvas_size.x, canvas_pos.y + canvas_size.y), IM_COL32(50,50,50,255), IM_COL32(50,50,60,255), IM_COL32(60,60,70,255), IM_COL32(50,50,60,255));
            auto uvs = o.intersection->uvs;
            auto uvRange = o.intersection->uvRange;

            for (int i = 0; i < uvs.size(); i++) {
                if (i == uvs.size() - 1 && !o.intersection->closed) break;
                auto uv0 = uvs[i];
                auto uv1 = uvs[(i+1)%uvs.size()];
                glm::ivec2 p0 = uv0 / uvRange * (float)size;
                glm::ivec2 p1 = uv1 / uvRange * (float)size;
                bool xflag = abs(p0.x - p1.x) > s2;
                bool yflag = abs(p0.y - p1.y) > s2;
                ImVec2 pp0 = canvas_pos;
                pp0.x += p0.x;
                pp0.y += p0.y;
                ImVec2 pp1 = canvas_pos;
                pp1.x += p1.x;
                pp1.y += p1.y;
                if (xflag || yflag) {
                    glm::ivec2 p02 = (p0 + p1) / 2;
                    glm::ivec2 p12 = (p0 + p1) / 2;
                    if (xflag) {
                        if (p0.x > s2) {
                            p02.x = size-1;
                            p12.x = 0;
                        } else {
                            p02.x = 0;
                            p12.x = size-1;
                        }
                    }
                    if (yflag) {
                        if (p0.y > s2) {
                            p02.y = size-1;
                            p12.y = 0;
                        } else {
                            p02.y = 0;
                            p12.y = size-1;
                        }
                    }
                    ImVec2 pp02 = canvas_pos;
                    pp02.x += p02.x;
                    pp02.y += p02.y;
                    ImVec2 pp12 = canvas_pos;
                    pp12.x += p12.x;
                    pp12.y += p12.y;
                    draw_list->AddLine(pp0, pp02, IM_COL32(255, 0, 0, 255), 1);
                    draw_list->AddLine(pp1, pp12, IM_COL32(255, 0, 0, 255), 1);
                } else {
                    draw_list->AddLine(pp0, pp1, IM_COL32(255, 0, 0, 255), 1);
                }
            }

            ImGui::Text("%s", o.intersection->uvTrim ? "Trimming" : "Not trimming");

            ImGui::Separator();

            ImGui::Text("%s", o.intersection->stName);
            canvas_pos = ImGui::GetCursorScreenPos();            // ImDrawList API uses screen coordinates!
            ImGui::InvisibleButton("canvas2", canvas_size);

            draw_list->AddRectFilledMultiColor(canvas_pos, ImVec2(canvas_pos.x + canvas_size.x, canvas_pos.y + canvas_size.y), IM_COL32(50,50,50,255), IM_COL32(50,50,60,255), IM_COL32(60,60,70,255), IM_COL32(50,50,60,255));
            uvs = o.intersection->sts;
            uvRange = o.intersection->stRange;

            for (int i = 0; i < uvs.size(); i++) {
                if (i == uvs.size() - 1 && !o.intersection->closed) break;
                auto uv0 = uvs[i];
                auto uv1 = uvs[(i+1)%uvs.size()];
                glm::ivec2 p0 = uv0 / uvRange * (float)size;
                glm::ivec2 p1 = uv1 / uvRange * (float)size;
                bool xflag = abs(p0.x - p1.x) > s2;
                bool yflag = abs(p0.y - p1.y) > s2;
                ImVec2 pp0 = canvas_pos;
                pp0.x += p0.x;
                pp0.y += p0.y;
                ImVec2 pp1 = canvas_pos;
                pp1.x += p1.x;
                pp1.y += p1.y;
                if (xflag || yflag) {
                    glm::ivec2 p02 = (p0 + p1) / 2;
                    glm::ivec2 p12 = (p0 + p1) / 2;
                    if (xflag) {
                        if (p0.x > s2) {
                            p02.x = size-1;
                            p12.x = 0;
                        } else {
                            p02.x = 0;
                            p12.x = size-1;
                        }
                    }
                    if (yflag) {
                        if (p0.y > s2) {
                            p02.y = size-1;
                            p12.y = 0;
                        } else {
                            p02.y = 0;
                            p12.y = size-1;
                        }
                    }
                    ImVec2 pp02 = canvas_pos;
                    pp02.x += p02.x;
                    pp02.y += p02.y;
                    ImVec2 pp12 = canvas_pos;
                    pp12.x += p12.x;
                    pp12.y += p12.y;
                    draw_list->AddLine(pp0, pp02, IM_COL32(0, 0, 255, 255), 1);
                    draw_list->AddLine(pp1, pp12, IM_COL32(0, 0, 255, 255), 1);
                } else {
                    draw_list->AddLine(pp0, pp1, IM_COL32(0, 0, 255, 255), 1);
                }
            }

            ImGui::Text("%s", o.intersection->stTrim ? "Trimming" : "Not trimming");

        }
        break;
    }

    if (!o.parents.size()) {
        ImGui::Separator();
        if (ImGui::Button("Delete")) {
            o.remove = true;
        }
    }
}

void cadence::Object::update(Object &o) {
    switch (o.type) {
        case OBJECT_POINT:
        case OBJECT_TORUS:
        case OBJECT_FLAKEC0:
        case OBJECT_FLAKEC2:
        case OBJECT_INTERSECTION:
            break;
        case OBJECT_BEZIERC0:
            o.bezierC0->points.remove_if([](Object* o) {return o->remove;});
            break;
        case OBJECT_BEZIERC2:
            {
                o.bezierC2->points.remove_if([](Object* o) {return o->remove;});

                size_t count;
                if (o.bezierC2->points.size() < 4) {
                    count = 0;
                } else {
                    count = (o.bezierC2->points.size() - 3) * 3 + 1;
                }

                if (o.bezierC2->virtuals.size() > count) {
                    for (size_t i = count; i < o.bezierC2->virtuals.size(); ++i)
                        o.bezierC2->virtuals[i].remove = true;
                } else if (o.bezierC2->virtuals.size() < count) {
                    for (size_t i = o.bezierC2->virtuals.size(); i < count; ++i) {
                        Object p = Object::createPoint({0, 0, 0}, true);
                        p.parents.push_back(&o);
                        o.bezierC2->virtuals.push_back(p);
                    }
                }

                if (!count) break;

                size_t counter = 0;
                auto iter0 = o.bezierC2->points.begin();
                auto iter1 = o.bezierC2->points.begin();
                auto iter2 = o.bezierC2->points.begin();
                auto it = o.bezierC2->virtuals.begin();
                ++iter1;
                ++iter2;
                ++iter2;
                std::vector<glm::vec3> points;

                while (iter2 != o.bezierC2->points.end()) {
                    glm::vec3 a = ((*iter0)->point->position + 2.0f * (*iter1)->point->position) / 3.0f;
                    glm::vec3 c = ((*iter2)->point->position + 2.0f * (*iter1)->point->position) / 3.0f;
                    glm::vec3 b = (a + c) / 2.0f;

                    if (counter != 0) {
                        (*it).point->position = a;
                        it++;
                    }
                    (*it).point->position = b;
                    it++;
                    if (counter != o.bezierC2->points.size() - 3) {
                        (*it).point->position = c;
                        it++;
                    }

                    ++iter0;
                    ++iter1;
                    ++iter2;
                    ++counter;
                }
            } break;
        case OBJECT_BEZIERC2I:
            o.bezierC0->points.remove_if([](Object* o) {return o->remove;});
            break;
        case OBJECT_GREGORY:
            for (int i = 0; i < o.parents.size(); i++) {
                if (o.parents[i]->remove) o.remove = true;
            }
            break;
    }
    if (o.parents.size()) {
        for (int i = 0; i < o.parents.size();) {
            if (o.parents[i]->remove) {
                o.parents[i] = o.parents[o.parents.size() - 1];
                o.parents.pop_back();
            } else {
                i++;
            }
        }
        if (!o.parents.size()) o.remove = true;
    }
}

void cadence::Object::cleanup(Object &o) {
    switch (o.type) {
        case OBJECT_BEZIERC2:
            {
                size_t count;
                if (o.bezierC2->points.size() < 4) {
                    count = 0;
                } else {
                    count = (o.bezierC2->points.size() - 3) * 3 + 1;
                }

                o.bezierC2->virtuals.resize(count);
            } break;
        case OBJECT_POINT:
        case OBJECT_TORUS:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2I:
        case OBJECT_FLAKEC0:
        case OBJECT_FLAKEC2:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            break;
    }
}

bool cadence::Object::isSurface() {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            return false;
        case OBJECT_TORUS:
        case OBJECT_FLAKEC0:
        case OBJECT_FLAKEC2:
            return true;
    }
    return false;
}

glm::vec2 cadence::Object::uvRange() {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
            return {glm::pi<float>() * 2, glm::pi<float>() * 2};
        case OBJECT_FLAKEC0:
            return {flakeC0->width, flakeC0->height};
        case OBJECT_FLAKEC2:
            return {flakeC2->width, flakeC2->height};
    }
    return {0, 0};
}

bool cadence::Object::uLoops() {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
            return true;
        case OBJECT_FLAKEC0:
            return flakeC0->cylinder;
        case OBJECT_FLAKEC2:
            return flakeC2->cylinder;
    }
    return false;
}

bool cadence::Object::vLoops() {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
            return true;
        case OBJECT_FLAKEC0:
        case OBJECT_FLAKEC2:
            return false;
    }
    return false;
}

static glm::vec4 bernstein3(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = ix * ix * ix;
    ret[1] = 3 * x * ix * ix;
    ret[2] = 3 * x * x * ix;
    ret[3] = x * x * x;
    return ret;
}

static glm::vec4 bernstein3d(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = -3 * ix * ix;
    ret[1] = 3 * ix * ix - 6 * x * ix;
    ret[2] = 6 * x * ix - 3 * x * x;
    ret[3] = 3 * x * x;
    return ret;
}

static glm::vec4 bspline3(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = ix * ix * ix / 6.f;
    ret[1] = (3.f * x  - 6.f) * x * x + 4;
    ret[1] /= 6.f;
    ret[2] = ix * 3 * x * x + 3 * x + 1;
    ret[2] /= 6.f;
    ret[3] = x * x * x / 6.f;
    return ret;
}

static glm::vec4 bspline3d(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = -3 * ix * ix / 6.f;
    ret[1] = 3 * x * (3 * x - 4);
    ret[1] /= 6.f;
    ret[2] = - 9 * x * x + 6 * x + 3;
    ret[2] /= 6.f;
    ret[3] = 3 * x * x / 6.f;
    return ret;
}

glm::vec3 cadence::Object::evaluate(glm::vec2 uv) {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
        {
            Torus &t = *torus;
            float x = (t.major + t.minor * cos(uv.x)) * cos(uv.y);
            float y = t.minor * sin(uv.x);
            float z = (t.major + t.minor * cos(uv.x)) * sin(uv.y);
            glm::vec3 pos = {x, y, z};
            glm::mat4 world = cadence::scale(t.scale);
            world *= cadence::rotateX(t.rotation.x);
            world *= cadence::rotateY(t.rotation.y);
            world *= cadence::rotateZ(t.rotation.z);
            world *= cadence::translate(t.position);
            pos = glm::vec3(glm::vec4(pos, 1) * world);
            return pos;
        }
        case OBJECT_FLAKEC0:
        {
            FlakeC0 &flake = *flakeC0;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[3 * w + i + (3 * flake.width + 1) * (h * 3 + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bernstein3(u);
            glm::vec4 tv = bernstein3(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
        case OBJECT_FLAKEC2:
        {
            FlakeC2 &flake = *flakeC2;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[w + i + (flake.width + 3) * (h + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bspline3(u);
            glm::vec4 tv = bspline3(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
            break;
    }
    return {0, 0, 0};
}

glm::vec3 cadence::Object::evaluateDu(glm::vec2 uv) {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
        {
            Torus &t = *torus;
            float x = -t.minor * sin(uv.x) * cos(uv.y);
            float y = t.minor * cos(uv.x);
            float z = -t.minor * sin(uv.x) * sin(uv.y);
            glm::vec3 pos = {x, y, z};
            glm::mat4 world = cadence::scale(t.scale);
            world *= cadence::rotateX(t.rotation.x);
            world *= cadence::rotateY(t.rotation.y);
            world *= cadence::rotateZ(t.rotation.z);
            world *= cadence::translate(t.position);
            pos = glm::vec3(glm::vec4(pos, 0) * world);
            return pos;
        }
        case OBJECT_FLAKEC0:
        {
            FlakeC0 &flake = *flakeC0;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[3 * w + i + (3 * flake.width + 1) * (h * 3 + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bernstein3d(u);
            glm::vec4 tv = bernstein3(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
        case OBJECT_FLAKEC2:
        {
            FlakeC2 &flake = *flakeC2;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[w + i + (flake.width + 3) * (h + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bspline3d(u);
            glm::vec4 tv = bspline3(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
            break;
    }
    return {0, 0, 0};
}

glm::vec3 cadence::Object::evaluateDv(glm::vec2 uv) {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
        {
            Torus &t = *torus;
            float x = (t.major + t.minor * cos(uv.x)) * -sin(uv.y);
            float y = 0;
            float z = (t.major + t.minor * cos(uv.x)) * cos(uv.y);
            glm::vec3 pos = {x, y, z};
            glm::mat4 world = cadence::scale(t.scale);
            world *= cadence::rotateX(t.rotation.x);
            world *= cadence::rotateY(t.rotation.y);
            world *= cadence::rotateZ(t.rotation.z);
            world *= cadence::translate(t.position);
            pos = glm::vec3(glm::vec4(pos, 0) * world);
            return pos;
        }
        case OBJECT_FLAKEC0:
        {
            FlakeC0 &flake = *flakeC0;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[3 * w + i + (3 * flake.width + 1) * (h * 3 + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bernstein3(u);
            glm::vec4 tv = bernstein3d(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
        case OBJECT_FLAKEC2:
        {
            FlakeC2 &flake = *flakeC2;
            int w = floor(uv.x);
            int h = floor(uv.y);
            float u = uv.x - w;
            float v = uv.y - h;

            if (w < 0) {
                w = 0;
                u = 0;
            }
            if (w >= flake.width) {
                w = flake.width - 1;
                u = 1;
            }
            if (h < 0) {
                h = 0;
                v = 0;
            }
            if (h >= flake.height) {
                h = flake.height - 1;
                v = 1;
            }

            glm::mat4 xs;
            glm::mat4 ys;
            glm::mat4 zs;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    glm::vec3 tempPos = flake.points[w + i + (flake.width + 3) * (h + j)]->point->position;
                    xs[i][j] = tempPos.x;
                    ys[i][j] = tempPos.y;
                    zs[i][j] = tempPos.z;
                }
            }

            glm::vec4 tu = bspline3(u);
            glm::vec4 tv = bspline3d(v);

            float x = dot(tv * xs, tu);
            float y = dot(tv * ys, tu);
            float z = dot(tv * zs, tu);
            return {x, y, z};
        }
            break;
    }
    return {0, 0, 0};
}

glm::vec3 cadence::Object::evaluateNormal(glm::vec2 uv) {
    return glm::cross(evaluateDu(uv), evaluateDv(uv));
}

glm::vec2 cadence::Object::uvFromPos(glm::vec3 pos, glm::vec2 target, float away) {
    switch (type) {
        case OBJECT_POINT:
        case OBJECT_BEZIERC0:
        case OBJECT_BEZIERC2:
        case OBJECT_BEZIERC2I:
        case OBJECT_GREGORY:
        case OBJECT_INTERSECTION:
            assert(false);
        case OBJECT_TORUS:
        {
            Torus &t = *torus;
            glm::mat4 world = cadence::scale(t.scale);
            world *= cadence::rotateX(t.rotation.x);
            world *= cadence::rotateY(t.rotation.y);
            world *= cadence::rotateZ(t.rotation.z);
            world *= cadence::translate(t.position);
            world = glm::inverse(world);
            pos = glm::vec3(glm::vec4(pos, 1) * world);
            float v = atan2(pos.z, pos.x);
            glm::mat4 r = cadence::rotateY(v);
            pos = glm::vec3(glm::vec4(pos, 1) * r);
            pos.x -= t.major;
            float u = atan2(pos.y, pos.x);
            if (away > 0) v += glm::pi<float>();
            if (v < 0) v += 2 * glm::pi<float>();
            if (u < 0) u += 2 * glm::pi<float>();
            return {u, v};
        }
        case OBJECT_FLAKEC0:
        case OBJECT_FLAKEC2:
        {
            glm::vec2 maxs = uvRange();
            glm::vec2 best = {0, 0};
            float dist = std::numeric_limits<float>::max();

            for (float u = 0; u < maxs.x; u += 0.0625) {
                for (float v = 0; v < maxs.y; v += 0.0625) {
                    if (glm::length(target - glm::vec2(u, v)) < away) continue;
                    glm::vec3 p = evaluate({u, v});
                    glm::vec3 diff = p - pos;
                    float d = glm::dot(diff, diff);
                    if (d < dist) {
                        dist = d;
                        best = {u, v};
                    }
                }
            }

            float startX = fmax(0, best.x - 0.0625);
            float startY = fmax(0, best.y - 0.0625);
            float endX = fmin(maxs.x, best.x + 0.0625);
            float endY = fmin(maxs.y, best.y + 0.0625);

            for (float u = startX; u < endX; u += 0.001) {
                for (float v = startY; v < endY; v += 0.001) {
                    if (glm::length(target - glm::vec2(u, v)) < away) continue;
                    glm::vec3 p = evaluate({u, v});
                    glm::vec3 diff = p - pos;
                    float d = glm::dot(diff, diff);
                    if (d < dist) {
                        dist = d;
                        best = {u, v};
                    }
                }
            }

            return best;
        }
            break;
    }
    return {0, 0};
}

glm::bvec2 cadence::Object::isOutside(glm::vec2 &uv) {
    auto range = uvRange();
    glm::bvec2 ret = {false, false};

    if (uv.x < 0) {
        if (uLoops()) {
            uv.x += range.x;
        } else {
            uv.x = 0;
            ret.x = true;
        }
    } else if (uv.x >= range.x) {
        if (uLoops()) {
            uv.x -= range.x;
        } else {
            uv.x = range.x - 0.0001f;
            ret.x = true;
        }
    }

    if (uv.y < 0) {
        if (vLoops()) {
            uv.y += range.y;
        } else {
            uv.y = 0;
            ret.y = true;
        }
    } else if (uv.y >= range.y) {
        if (vLoops()) {
            uv.y -= range.y;
        } else {
            uv.y = range.y - 0.0001f;
            ret.y = true;
        }
    }

    return ret;
}

glm::vec4 cadence::gradient(Object &o1, Object &o2, glm::vec2 uv, glm::vec2 st) {
    glm::vec3 e1 = o1.evaluate(uv);
    glm::vec3 e2 = o2.evaluate(st);
    glm::vec3 ed = e1 - e2;

    glm::vec3 u1 = o1.evaluateDu(uv);
    glm::vec3 v1 = o1.evaluateDv(uv);

    float u1l = glm::dot(u1, u1);
    float v1l = glm::dot(v1, v1);

    glm::vec3 u2 = o2.evaluateDu(st);
    glm::vec3 v2 = o2.evaluateDv(st);

    float u2l = glm::dot(u2, u2);
    float v2l = glm::dot(v2, v2);

    u1 /= u1l;
    v1 /= v1l;
    u2 /= u2l;
    v2 /= v2l;

    return glm::vec4(glm::dot( ed, u1),
                     glm::dot( ed, v1),
                     glm::dot(-ed, u2),
                     glm::dot(-ed, v2));
}

glm::mat4 cadence::jacobi(Object &o1, Object &o2, glm::vec2 uv, glm::vec2 st, glm::vec3 planeNormal) {
    auto du = o1.evaluateDu(uv);
    auto dv = o1.evaluateDv(uv);
    auto ds = o2.evaluateDu(st);
    auto dt = o2.evaluateDv(st);

    glm::vec4 column0 = {du, glm::dot(du, planeNormal)};
    glm::vec4 column1 = {dv, glm::dot(dv, planeNormal)};
    glm::vec4 column2 = {-ds, 0};
    glm::vec4 column3 = {-dt, 0};

    glm::mat4 ret = {column0, column1, column2, column3};

    return ret;
}

